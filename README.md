# iSCAN Standard app for Android

## Customize for new event

To create app for new event:

1. Fork the [standard repo](https://bitbucket.org/nhat-corp-it/standard-iscan-android/)

1. Change app icon

1. Change variables in gradle.properties
    - application_id: package name to put on google play. Should follow format: com.corpit.iscan{eventname}{year}
    - api_root_url: url to the api which usually ends before "MasterAPI"
    - decode_key: for decrypting QR code
    - show_id: for retrieving data for each event
    - enable_exhibitor_login, enable_visitor_login: enable specific account type for each event
    - release_store_file: route to the key file
    - demo_store_file: route to key file for signing demo apk

## Build variants

There are three build types:

- debug: for development
- demo: for sending to client
- release: for upload to play store

How to change build variant: <https://stackoverflow.com/a/45746101/8575792>