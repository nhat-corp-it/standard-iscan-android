package com.corpit.iscan.di.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.corpit.iscan.BuildConfig;
import com.corpit.iscan.global.AppExecutors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static android.content.Context.MODE_PRIVATE;

@Module
public class AppModule {

    private Application app;

    public AppModule(Application app) {
        this.app = app;
    }

    @Provides
    @Singleton
    Context appContext() {
        return app.getApplicationContext();
    }

    @Provides
    @Singleton
    AppExecutors appExecutors() {
        return new AppExecutors();
    }

    @Provides
    @Singleton
    SharedPreferences sharedPreferences() {
        return app.getSharedPreferences(BuildConfig.APPLICATION_ID, MODE_PRIVATE);
    }

}
