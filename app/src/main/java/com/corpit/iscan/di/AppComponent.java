package com.corpit.iscan.di;

import android.content.Context;
import android.content.SharedPreferences;

import com.corpit.iscan.data.ProductRepository;
import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.data.remote.IScanService;
import com.corpit.iscan.di.module.AppModule;
import com.corpit.iscan.di.module.DatabaseModule;
import com.corpit.iscan.di.module.ServiceModule;
import com.corpit.iscan.di.module.UserModule;
import com.corpit.iscan.global.AppExecutors;
import com.corpit.iscan.global.UserManager;
import com.corpit.iscan.viewmodel.LoginViewModel;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {AppModule.class, DatabaseModule.class, ServiceModule.class, UserModule.class})
@Singleton
public interface AppComponent {

    Context appContext();

    AppExecutors appExecutors();

    SharedPreferences sharedPreferences();

    AppDatabase db();

    IScanService iscanService();

    ProductRepository productRepo();

    UserManager userManager();

}
