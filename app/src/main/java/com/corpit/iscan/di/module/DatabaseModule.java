package com.corpit.iscan.di.module;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.data.db.dao.ActionDao;
import com.corpit.iscan.data.db.dao.BoothDao;
import com.corpit.iscan.data.db.dao.ExhibitorProductDao;
import com.corpit.iscan.data.db.dao.ProductDao;
import com.corpit.iscan.data.db.dao.VisitorDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {AppModule.class})
public class DatabaseModule {

    private static final String DATABASE_NAME = "Standard2018v1.db";

    @Provides
    @Singleton
    AppDatabase db(Context context) {
        return Room.databaseBuilder(context.getApplicationContext(),
                AppDatabase.class, DATABASE_NAME).build();
    }

    @Provides
    @Singleton
    VisitorDao visitorDao(AppDatabase db) {
        return db.visitorDao();
    }

    @Provides
    @Singleton
    BoothDao boothDao(AppDatabase db) {
        return db.boothDao();
    }

    @Provides
    @Singleton
    ActionDao actionDao(AppDatabase db) {
        return db.actionDao();
    }

    @Provides
    @Singleton
    ExhibitorProductDao exhibitorProductDao(AppDatabase db) {
        return db.exhibitorProductDao();
    }

    @Provides
    @Singleton
    ProductDao productDao(AppDatabase db) {
        return db.productDao();
    }
}
