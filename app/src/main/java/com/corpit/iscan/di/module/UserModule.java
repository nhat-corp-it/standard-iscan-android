package com.corpit.iscan.di.module;

import android.content.SharedPreferences;

import com.corpit.iscan.global.UserManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    @Provides
    @Singleton
    UserManager userManager(SharedPreferences sharedPreferences) {
        return new UserManager(sharedPreferences);
    }
}
