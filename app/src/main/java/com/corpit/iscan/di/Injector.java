package com.corpit.iscan.di;

import com.corpit.iscan.global.IScanApp;

public class Injector {

    public static AppComponent get() {
        return IScanApp.get().getComponent();
    }
}
