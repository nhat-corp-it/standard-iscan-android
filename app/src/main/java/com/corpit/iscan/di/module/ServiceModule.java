package com.corpit.iscan.di.module;

import com.corpit.iscan.BuildConfig;
import com.corpit.iscan.data.remote.IScanService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
public class ServiceModule {

    private static final String ROOT_URL = BuildConfig.API_ROOT_URL;

    @Provides
    @Singleton
    Retrofit retrofit() {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(20, TimeUnit.SECONDS)
                .connectTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .build();

        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    IScanService iscanService(Retrofit retrofit) {
        return retrofit.create(IScanService.class);
    }
}
