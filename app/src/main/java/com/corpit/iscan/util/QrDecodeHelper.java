package com.corpit.iscan.util;

import android.support.annotation.IntDef;
import android.util.Base64;

import com.corpit.iscan.BuildConfig;
import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.User;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.model.Visitor;

import java.io.UnsupportedEncodingException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class QrDecodeHelper {

    private static final int DECODE_KEY = BuildConfig.DECODE_KEY;

    @IntDef({QrCodeType.EXHIBITOR, QrCodeType.VISITOR, QrCodeType.BOOTH})
    @Retention(RetentionPolicy.SOURCE)
    public @interface QrCodeType {
        int EXHIBITOR = 5;
        int VISITOR = 10;
        int BOOTH = 15;
    }

    private static final String SEPARATOR = "^";

    private static final String SEPARATOR_REGEX = "\\^";

    private QrDecodeHelper() {
    }

    public static String encrypt(String string) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            char tempChar = string.charAt(i);
            if ((int) tempChar + DECODE_KEY > 126) {
                result.append((char) ((((int) tempChar + DECODE_KEY) - 127) + 32));
            } else {
                result.append((char) ((int) tempChar + DECODE_KEY));
            }
        }

        return result.toString();
    }

    public static String newEncrypt(String string, String key) throws Exception {
        byte[] sha = sha384(key);
        byte[] shaKey = Arrays.copyOfRange(sha, 0, 32);
        byte[] iv = Arrays.copyOfRange(sha, 32, 48);

        SecretKeySpec skeySpec = new SecretKeySpec(shaKey, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(iv));

        byte[] encrypted = cipher.doFinal(string.getBytes());
        return Base64.encodeToString(encrypted, Base64.DEFAULT);
    }

    public static String decrypt(String string) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            char tempChar = string.charAt(i);
            if ((int) tempChar - DECODE_KEY < 32) {
                result.append((char) ((((int) tempChar - DECODE_KEY) + 127) - 32));
            } else {
                result.append((char) ((int) tempChar - DECODE_KEY));
            }
        }

        return result.toString();
    }

    public static String newDecrypt(String string, String key) throws Exception {
        byte[] sha = sha384(key);
        byte[] shaKey = Arrays.copyOfRange(sha, 0, 32);
        byte[] iv = Arrays.copyOfRange(sha, 32, 48);

        SecretKeySpec skeySpec = new SecretKeySpec(shaKey, "AES");
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, new IvParameterSpec(iv));

        byte[] decrypted = cipher.doFinal(Base64.decode(string, Base64.DEFAULT));
        return new String(decrypted);
    }

    private static byte[] sha384(String input) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md5 = MessageDigest.getInstance("SHA-384");

        return md5.digest(input.getBytes("UTF-8"));
    }

    public static boolean isValidQrCode(String decodedString, @QrCodeType int type) {
        switch (type) {
            case QrCodeType.EXHIBITOR:
                return decodedString.startsWith("e") && decodedString.split(SEPARATOR_REGEX).length == 8;
            case QrCodeType.VISITOR:
                return decodedString.startsWith("v") && decodedString.split(SEPARATOR_REGEX).length == 7;
            case QrCodeType.BOOTH:
                return decodedString.startsWith("b") && decodedString.split(SEPARATOR_REGEX).length == 7;
            default:
                throw new IllegalArgumentException("Scan type not supported.");
        }
    }

    //Ex: eNathan Ton^info@corpit.com.sg^CorpIT^Singapore^Manager^65 6565 6565^RegNo0001^RegNo0001
    public static User toUser(String decodedString) {
        String[] fields = decodedString.substring(1).split(SEPARATOR_REGEX);
        User user = new User();
        user.setName(fields[0]);
        user.setEmail(fields[1]);
        user.setCompany(fields[2]);
        user.setCountry(fields[3]);
        user.setJobTitle(fields[4]);
        user.setPhone(fields[5]);
        user.setRegisterId(fields[6]);
        if (fields.length == 8) {
            user.setCompanyId(fields[7]);
            user.setType(UserType.EXHIBITOR);
        } else {
            user.setType(UserType.VISITOR);
        }

        user.setQrCode(decodedString);

        return user;
    }

    public static Visitor toVisitor(String decodedString) {
        String[] fields = decodedString.substring(1).split(SEPARATOR_REGEX);
        Visitor visitor = new Visitor();
        visitor.setName(fields[0]);
        visitor.setEmail(fields[1]);
        visitor.setCompany(fields[2]);
        visitor.setCountry(fields[3]);
        visitor.setJobTitle(fields[4]);
        visitor.setPhone(fields[5]);
        visitor.setRegisterId(fields[6]);

        visitor.setQrCode(decodedString);

        return visitor;
    }

    //Ex: bCorpIT^B1-1^www.corpit.com.sg^p1#p1#p3^Company Introduction^Singapore^1
    public static Booth toBooth(String decodedString) {
        String[] fields = decodedString.substring(1).split(SEPARATOR_REGEX);
        Booth booth = new Booth();
        booth.setCompany(fields[0]);
        booth.setBooth(fields[1]);
        booth.setWebsite(fields[2]);
        booth.setProducts(fields[3]);
        booth.setDescription(fields[4]);
        booth.setCountry(fields[5]);
        booth.setCompanyId(fields[6]);

        booth.setQrCode(decodedString);

        return booth;
    }

    public static String toQrCode(Visitor visitor) {
        return "v" + visitor.getName() +
                SEPARATOR +
                visitor.getEmail() +
                SEPARATOR +
                visitor.getCompany() +
                SEPARATOR +
                visitor.getCountry() +
                SEPARATOR +
                visitor.getJobTitle() +
                SEPARATOR +
                visitor.getPhone() +
                SEPARATOR +
                visitor.getRegisterId();
    }

    public static String toQrCode(Booth booth) {
        return "b" + booth.getCompany() +
                SEPARATOR +
                booth.getBooth() +
                SEPARATOR +
                booth.getWebsite() +
                SEPARATOR +
                booth.getProducts() +
                SEPARATOR +
                booth.getDescription() +
                SEPARATOR +
                booth.getCountry() +
                SEPARATOR +
                booth.getCompanyId();
    }
}
