package com.corpit.iscan.util;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.corpit.iscan.R;
import com.corpit.iscan.ui.base.BaseActivity;

import static android.app.Activity.RESULT_OK;

public final class AlertHelper {

    private AlertHelper() {
    }

    public static void showLogoutDialog(final BaseActivity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(R.string.title_log_out)
                .setMessage(R.string.msg_log_out)
                .setPositiveButton(R.string.action_log_out, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.setResult(RESULT_OK);
                        activity.finish();
                    }
                })
                .setNegativeButton(R.string.action_cancel, null);

        builder.create().show();
    }

    public static void showGoBackDialog(final BaseActivity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.title_cancel_insert)
                .setMessage(R.string.msg_cancel_insert)
                .setPositiveButton(R.string.action_not_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        activity.finish();
                    }
                })
                .setNegativeButton(R.string.action_cancel, null);
        builder.create().show();
    }

}
