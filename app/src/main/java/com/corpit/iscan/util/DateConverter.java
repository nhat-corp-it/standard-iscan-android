package com.corpit.iscan.util;

import com.corpit.iscan.model.Show;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class DateConverter {

    public static final DateFormat SERVER_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-DD HH:mm:ss.SSS ZZZZ", Locale.UK);
    public static final DateFormat CLIENT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.UK);

    public static final DateFormat SHOW_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa", Locale.UK);

    private DateConverter() {
    }

    public static String toText(long timestamp, DateFormat dateFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);

        return dateFormat.format(calendar.getTime());
    }

    public static String toText(Show show) {
        Calendar start = Calendar.getInstance();
        Calendar end = Calendar.getInstance();
        try {
            Date startDate = SHOW_DATE_FORMAT.parse(show.getStartDate());
            Date endDate = SHOW_DATE_FORMAT.parse(show.getEndDate());
            start.setTime(startDate);
            end.setTime(endDate);

            int startMonth = start.get(Calendar.MONTH);
            int startYear = start.get(Calendar.YEAR);

            int endMonth = end.get(Calendar.MONTH);
            int endYear = end.get(Calendar.YEAR);

            if (startYear != endYear) {
                return MessageFormat.format("{0,date,dd MMM yyyy} - {1,date,dd MMM yyyy}", startDate, endDate);
            } else if (startMonth != endMonth) {
                return MessageFormat.format("{0,date,dd MMM} - {1,date,dd MMM yyyy}", startDate, endDate);
            } else {
                return MessageFormat.format("{0,date,dd} - {1,date,dd MMM yyyy}", startDate, endDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
