package com.corpit.iscan.util;

import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.User;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.model.Visitor;

public final class DummyGenerator {

    public static Booth dummyBooth() {
        Booth booth = new Booth();
        booth.setCompany("CorpIT");
        booth.setBooth("B1-1");
        booth.setWebsite("www.corpit.com.sg");
        booth.setProducts("P10001#P10002#P10003");
        booth.setDescription("Company Introduction");
        booth.setCountry("Singapore");
        booth.setCompanyId("RegNo0001");
        booth.setQrCode("bCorpIT^B1-1^www.corpit.com.sg^P10001#P10003#P10005^Company Introduction^Singapore^RegNo0001");

        return booth;
    }

    public static Visitor dummyVisitor() {
        Visitor visitor = new Visitor();
        visitor.setName("INNA SUN ");
        visitor.setEmail("inna.sun@ul.com");
        visitor.setCompany("UL ENVIRONMENT");
        visitor.setCountry("SINGAPORE");
        visitor.setJobTitle("ASEAN BDM");
        visitor.setPhone("65 9640 7659");
        visitor.setRegisterId("767");
        visitor.setQrCode("vINNA SUN ^inna.sun@ul.com^UL ENVIRONMENT^SINGAPORE^ASEAN BDM^65 9640 7659^767");

        return visitor;
    }

    public static User dummyUser(@UserType String type) {
        User user = new User();
        user.setName("INNA SUN ");
        user.setEmail("inna.sun@ul.com");
        user.setCompany("UL ENVIRONMENT");
        user.setCountry("SINGAPORE");
        user.setJobTitle("ASEAN BDM");
        user.setPhone("65 9640 7659");
        user.setRegisterId("767");
        user.setType(type);
        switch (type) {
            case UserType.EXHIBITOR:
                user.setCompanyId("RegNo0001");
                user.setQrCode("vINNA SUN ^inna.sun@ul.com^UL ENVIRONMENT^SINGAPORE^ASEAN BDM^65 9640 7659^767^RegNo0001");
                return user;
            case UserType.VISITOR:
                user.setQrCode("vINNA SUN ^inna.sun@ul.com^UL ENVIRONMENT^SINGAPORE^ASEAN BDM^65 9640 7659^767");
                return user;
            default:
                return null;
        }
    }
}
