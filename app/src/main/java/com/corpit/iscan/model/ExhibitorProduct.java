package com.corpit.iscan.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;

import com.squareup.moshi.Json;

@Entity(primaryKeys = {"product_name", "company_id"})
public class ExhibitorProduct {

    @NonNull
    @Json(name = "pro_name")
    @ColumnInfo(name = "product_name")
    private String name;

    @NonNull
    @ColumnInfo(name = "company_id")
    private transient String companyId;

    public ExhibitorProduct(@NonNull String name, @NonNull String companyId) {
        this.name = name;
        this.companyId = companyId;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(@NonNull String companyId) {
        this.companyId = companyId;
    }
}
