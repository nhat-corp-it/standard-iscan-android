package com.corpit.iscan.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.corpit.iscan.ui.base.IRecord;

@Entity
public class Visitor implements IRecord, Parcelable {

    @PrimaryKey(autoGenerate = true)
    private long id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "email")
    private String email;

    @ColumnInfo(name = "company")
    private String company;

    @ColumnInfo(name = "country")
    private String country;

    @ColumnInfo(name = "job_title")
    private String jobTitle;

    @ColumnInfo(name = "phone")
    private String phone;

    @ColumnInfo(name = "register_id")
    private String registerId;

    @ColumnInfo(name = "products")
    private String products;

    @ColumnInfo(name = "actions")
    private String actions;

    @ColumnInfo(name = "qr_code")
    private String qrCode;

    @ColumnInfo(name = "scanner_id")
    private String scannerId;

    @ColumnInfo(name = "timestamp")
    private long timestamp;

    @ColumnInfo(name = "remark")
    private String remark = "";

    @ColumnInfo(name = "sent")
    private boolean sent;

    @Ignore
    public Visitor(long id, String name, String email, String company, String country, String jobTitle, String phone, String registerId, String products, String actions, String qrCode, String scannerId, long timestamp, String remark, boolean sent) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.company = company;
        this.country = country;
        this.jobTitle = jobTitle;
        this.phone = phone;
        this.registerId = registerId;
        this.products = products;
        this.actions = actions;
        this.qrCode = qrCode;
        this.scannerId = scannerId;
        this.timestamp = timestamp;
        this.remark = remark;
        this.sent = sent;
    }

    public Visitor(){}

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    @Override
    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getScannerId() {
        return scannerId;
    }

    @Override
    public void setScannerId(String scannerId) {
        this.scannerId = scannerId;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean isSent() {
        return sent;
    }

    @Override
    public void setSent(boolean sent) {
        this.sent = sent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.company);
        dest.writeString(this.country);
        dest.writeString(this.jobTitle);
        dest.writeString(this.phone);
        dest.writeString(this.registerId);
        dest.writeString(this.products);
        dest.writeString(this.actions);
        dest.writeString(this.qrCode);
        dest.writeString(this.scannerId);
        dest.writeLong(this.timestamp);
        dest.writeString(this.remark);
        dest.writeByte(this.sent ? (byte) 1 : (byte) 0);
    }

    @Ignore
    protected Visitor(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.email = in.readString();
        this.company = in.readString();
        this.country = in.readString();
        this.jobTitle = in.readString();
        this.phone = in.readString();
        this.registerId = in.readString();
        this.products = in.readString();
        this.actions = in.readString();
        this.qrCode = in.readString();
        this.scannerId = in.readString();
        this.timestamp = in.readLong();
        this.remark = in.readString();
        this.sent = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Visitor> CREATOR = new Parcelable.Creator<Visitor>() {
        @Override
        public Visitor createFromParcel(Parcel source) {
            return new Visitor(source);
        }

        @Override
        public Visitor[] newArray(int size) {
            return new Visitor[size];
        }
    };
}
