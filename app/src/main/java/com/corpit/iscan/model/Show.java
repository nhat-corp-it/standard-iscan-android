package com.corpit.iscan.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

@Entity
public class Show implements Parcelable {

    @Json(name = "venue")
    private String venue;

    @Json(name = "coordinatorID")
    private String coordinatorID;

    @Json(name = "showName")
    private String showName;

    @PrimaryKey
    @Json(name = "showID")
    private String showID;

    @Json(name = "showEncryptedKey")
    private String showEncryptedKey;

    @Json(name = "deleteFlag")
    private String deleteFlag;

    @Json(name = "showlogo")
    private String showLogo;

    @Json(name = "showbanner")
    private String showBanner;

    @Json(name = "startDate")
    private String startDate;

    @Json(name = "createdDate")
    private String createdDate;

    @Json(name = "showClosingMessage")
    private String showClosingMessage;

    @Json(name = "visitorLogin")
    private String visitorLogin;

    @Json(name = "updatedDate")
    private String updatedDate;

    @Json(name = "isLive")
    private String isLive;

    @Json(name = "endDate")
    private String endDate;

    @Json(name = "organizer")
    private String organizer;

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getCoordinatorID() {
        return coordinatorID;
    }

    public void setCoordinatorID(String coordinatorID) {
        this.coordinatorID = coordinatorID;
    }

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName;
    }

    public String getShowID() {
        return showID;
    }

    public void setShowID(String showID) {
        this.showID = showID;
    }

    public String getShowEncryptedKey() {
        return showEncryptedKey;
    }

    public void setShowEncryptedKey(String showEncryptedKey) {
        this.showEncryptedKey = showEncryptedKey;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getShowLogo() {
        return showLogo;
    }

    public void setShowLogo(String showLogo) {
        this.showLogo = showLogo;
    }

    public String getShowBanner() {
        return showBanner;
    }

    public void setShowBanner(String showBanner) {
        this.showBanner = showBanner;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getShowClosingMessage() {
        return showClosingMessage;
    }

    public void setShowClosingMessage(String showClosingMessage) {
        this.showClosingMessage = showClosingMessage;
    }

    public String getVisitorLogin() {
        return visitorLogin;
    }

    public void setVisitorLogin(String visitorLogin) {
        this.visitorLogin = visitorLogin;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getIsLive() {
        return isLive;
    }

    public void setIsLive(String isLive) {
        this.isLive = isLive;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.showID);
        dest.writeString(this.showEncryptedKey);
        dest.writeString(this.showBanner);
        dest.writeString(this.showLogo);
        dest.writeString(this.visitorLogin);
    }

    public Show() {
    }

    protected Show(Parcel in) {
        this.showID = in.readString();
        this.showEncryptedKey = in.readString();
        this.showBanner = in.readString();
        this.showLogo = in.readString();
        this.visitorLogin = in.readString();
    }

    public static final Parcelable.Creator<Show> CREATOR = new Parcelable.Creator<Show>() {
        @Override
        public Show createFromParcel(Parcel source) {
            return new Show(source);
        }

        @Override
        public Show[] newArray(int size) {
            return new Show[size];
        }
    };
}
