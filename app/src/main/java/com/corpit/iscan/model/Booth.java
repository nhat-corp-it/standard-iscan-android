package com.corpit.iscan.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.corpit.iscan.ui.base.IRecord;

@Entity
public class Booth implements IRecord, Parcelable {

    @PrimaryKey(autoGenerate = true)
    private long id;

    private String company;

    private String booth;

    private String website;

    private String products;

    private String description;

    private String country;

    @ColumnInfo(name = "company_id")
    private String companyId;

    @ColumnInfo(name = "qr_code")
    private String qrCode;

    @ColumnInfo(name = "scanner_id")
    private String scannerId;

    @ColumnInfo(name = "timestamp")
    private long timestamp;

    @ColumnInfo(name = "remark")
    private String remark;

    @ColumnInfo(name = "sent")
    private boolean sent;

    @Ignore
    public Booth(long id, String company, String booth, String website, String products,
                 String description, String country, String companyId, String qrCode, String scannerId,
                 long timestamp, String remark, boolean sent) {
        this.id = id;
        this.company = company;
        this.booth = booth;
        this.website = website;
        this.products = products;
        this.description = description;
        this.country = country;
        this.companyId = companyId;
        this.qrCode = qrCode;
        this.scannerId = scannerId;
        this.timestamp = timestamp;
        this.remark = remark;
        this.sent = sent;
    }

    public Booth() {
    }

    @Override
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBooth() {
        return booth;
    }

    public void setBooth(String booth) {
        this.booth = booth;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @Override
    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getScannerId() {
        return scannerId;
    }

    @Override
    public void setScannerId(String scannerId) {
        this.scannerId = scannerId;
    }

    @Override
    public String getName() {
        return company;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean isSent() {
        return sent;
    }

    @Override
    public void setSent(boolean sent) {
        this.sent = sent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.company);
        dest.writeString(this.booth);
        dest.writeString(this.website);
        dest.writeString(this.products);
        dest.writeString(this.description);
        dest.writeString(this.country);
        dest.writeString(this.companyId);
        dest.writeString(this.qrCode);
        dest.writeString(this.scannerId);
        dest.writeLong(this.timestamp);
        dest.writeString(this.remark);
        dest.writeByte(this.sent ? (byte) 1 : (byte) 0);
    }

    @Ignore
    protected Booth(Parcel in) {
        this.id = in.readLong();
        this.company = in.readString();
        this.booth = in.readString();
        this.website = in.readString();
        this.products = in.readString();
        this.description = in.readString();
        this.country = in.readString();
        this.companyId = in.readString();
        this.qrCode = in.readString();
        this.scannerId = in.readString();
        this.timestamp = in.readLong();
        this.remark = in.readString();
        this.sent = in.readByte() != 0;
    }

    public static final Creator<Booth> CREATOR = new Creator<Booth>() {
        @Override
        public Booth createFromParcel(Parcel source) {
            return new Booth(source);
        }

        @Override
        public Booth[] newArray(int size) {
            return new Booth[size];
        }
    };
}
