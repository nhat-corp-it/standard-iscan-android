package com.corpit.iscan.model;

import com.squareup.moshi.Json;

public class Guide {

    @Json(name = "HTMLString")
    private String htmlString;

    public Guide(String htmlString) {
        this.htmlString = htmlString;
    }

    public String getHtmlString() {
        return htmlString;
    }
}
