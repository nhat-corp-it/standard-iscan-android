package com.corpit.iscan.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.squareup.moshi.Json;

@Entity
public class Action {

    @PrimaryKey
    @NonNull
    @Json(name = "ActionID")
    private String id;

    @Json(name = "ActionName")
    private String name;

    @Ignore
    public Action(@NonNull String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Action() {
    }

    @NonNull
    public String getId() {
        return this.id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
