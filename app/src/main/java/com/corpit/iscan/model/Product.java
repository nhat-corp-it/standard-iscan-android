package com.corpit.iscan.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.squareup.moshi.Json;

@Entity
public class Product {

    @PrimaryKey(autoGenerate = true)
    private Long _id;

    @Json(name = "prod_ID")
    private String id;

    @Json(name = "prod_name")
    private String name;

    @Json(name = "prod_desc")
    private String description;

    @Json(name = "prod_code")
    private String code;

    public Product(Long _id, String id, String name, String description, String code) {
        this._id = _id;
        this.id = id;
        this.name = name;
        this.description = description;
        this.code = code;
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
