package com.corpit.iscan.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class User implements Parcelable {

    @StringDef({UserType.EXHIBITOR, UserType.VISITOR})
    @Retention(RetentionPolicy.SOURCE)
    public @interface UserType {
        String EXHIBITOR = "e";
        String VISITOR = "v";
    }

    private String name;

    private String email;

    private String company;

    private String country;

    private String jobTitle;

    private String phone;

    private String registerId;

    private String companyId;

    @UserType
    private String type;

    private String qrCode;

    public User(String name, String email, String company, String country, String jobTitle,
                String phone, String registerId, String companyId, String type, String qrCode) {
        this.name = name;
        this.email = email;
        this.company = company;
        this.country = country;
        this.jobTitle = jobTitle;
        this.phone = phone;
        this.registerId = registerId;
        this.companyId = companyId;
        this.type = type;
        this.qrCode = qrCode;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRegisterId() {
        return registerId;
    }

    public void setRegisterId(String registerId) {
        this.registerId = registerId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public @UserType
    String getType() {
        return type;
    }

    public void setType(@UserType String type) {
        this.type = type;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.company);
        dest.writeString(this.country);
        dest.writeString(this.jobTitle);
        dest.writeString(this.phone);
        dest.writeString(this.registerId);
        dest.writeString(this.companyId);
        dest.writeString(this.type);
        dest.writeString(this.qrCode);
    }

    protected User(Parcel in) {
        this.name = in.readString();
        this.email = in.readString();
        this.company = in.readString();
        this.country = in.readString();
        this.jobTitle = in.readString();
        this.phone = in.readString();
        this.registerId = in.readString();
        this.companyId = in.readString();
        this.type = in.readString();
        this.qrCode = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
