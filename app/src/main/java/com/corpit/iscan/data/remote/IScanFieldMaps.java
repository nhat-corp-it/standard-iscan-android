package com.corpit.iscan.data.remote;

import android.bluetooth.BluetoothAdapter;

import com.corpit.iscan.di.Injector;
import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.model.Visitor;
import com.corpit.iscan.util.DateConverter;

import java.util.HashMap;
import java.util.Map;

import static com.corpit.iscan.util.DateConverter.SERVER_DATE_FORMAT;

public final class IScanFieldMaps {

    public static Map<String, String> login(@UserType String type, String qrCode) {
        Map<String, String> map = new HashMap<>();
        switch (type) {
            case UserType.EXHIBITOR:
                map.put("type", "eLogin");
                break;
            case UserType.VISITOR:
                map.put("type", "vLogin");
                break;
            default:
                throw new IllegalArgumentException("Wrong user type");
        }
        map.put("showid", Injector.get().userManager().getShowId());
        map.put("QRCode", qrCode);

        return map;
    }

    public static Map<String, String> actions() {
        Map<String, String> map = new HashMap<>();
        map.put("type", "refAction");
        map.put("showid", Injector.get().userManager().getShowId());

        return map;
    }

    public static Map<String, String> products() {
        Map<String, String> map = new HashMap<>();
        map.put("type", "product");
        map.put("showid", Injector.get().userManager().getShowId());

        return map;
    }

    public static Map<String, String> exhibitorProducts(String companyId) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "ExhibitorProduct");
        map.put("showid", Injector.get().userManager().getShowId());
        map.put("CompiD", companyId);

        return map;
    }

    public static Map<String, String> guide(String type) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "Guide");
        map.put("showid", Injector.get().userManager().getShowId());
        switch (type) {
            case UserType.EXHIBITOR:
                map.put("UserType", "e");
                break;
            case UserType.VISITOR:
                map.put("UserType", "v");
                break;
            default:
                throw new IllegalArgumentException("Wrong user type");
        }

        return map;
    }

    public static Map<String, String> syncVisitors(Visitor visitor) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "upScanV");
        map.put("showid", Injector.get().userManager().getShowId());
        map.put("Product", visitor.getProducts());
        map.put("Remark", visitor.getRemark());
        map.put("Action", visitor.getActions());
        map.put("Date", DateConverter.toText(visitor.getTimestamp(), SERVER_DATE_FORMAT));
        map.put("LoginID", Injector.get().userManager().getUser().getRegisterId());
        map.put("QRCode", visitor.getQrCode());
        map.put("DeviceName", BluetoothAdapter.getDefaultAdapter().getName());

        return map;
    }

    public static Map<String, String> syncBooths(Booth booth) {
        Map<String, String> map = new HashMap<>();
        map.put("type", "upScanB");
        map.put("showid", Injector.get().userManager().getShowId());
        map.put("QRCode", booth.getQrCode());
        map.put("remark", booth.getRemark());
        map.put("vregno", Injector.get().userManager().getUser().getRegisterId());
        map.put("Date", DateConverter.toText(booth.getTimestamp(), SERVER_DATE_FORMAT));
        map.put("DeviceName", BluetoothAdapter.getDefaultAdapter().getName());

        return map;
    }

    public static Map<String, String> shows() {
        Map<String, String> map = new HashMap<>();
        map.put("type", "showlist");

        return map;
    }
}
