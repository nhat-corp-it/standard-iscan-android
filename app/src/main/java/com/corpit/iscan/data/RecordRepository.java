package com.corpit.iscan.data;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.data.db.dao.RecordDao;
import com.corpit.iscan.data.remote.IScanClient;
import com.corpit.iscan.data.remote.IScanFieldMaps;
import com.corpit.iscan.data.remote.response.IScanResponse;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.global.AppExecutors;
import com.corpit.iscan.model.Action;
import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.ExhibitorProduct;
import com.corpit.iscan.model.Guide;
import com.corpit.iscan.model.Product;
import com.corpit.iscan.model.User;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.model.Visitor;
import com.corpit.iscan.ui.base.IRecord;

import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.List;

import retrofit2.Response;

import static java.lang.System.currentTimeMillis;

public class RecordRepository<T extends IRecord> implements RecordsDataSource<T> {

    private static RecordRepository sInstance;

    private final RecordDao<T> mRecordDao;

    private final AppExecutors mAppExecutors;

    private RecordRepository(@NonNull AppExecutors appExecutors, @NonNull RecordDao<T> recordDao) {
        mAppExecutors = appExecutors;
        mRecordDao = recordDao;
    }

    public static <T extends IRecord> RecordRepository<T> getInstance(@NonNull AppExecutors appExecutors,
                                                                      @NonNull RecordDao recordDao) {
        if (sInstance == null) {
            synchronized (RecordRepository.class) {
                if (sInstance == null) {
                    sInstance = new RecordRepository<>(appExecutors, recordDao);
                }
            }
        }
        return sInstance;
    }

    public void reset() {
        sInstance = null;
    }

    @Override
    public LiveData<List<T>> getRecords() {
        return mRecordDao.loadAll();
    }

    @Override
    public void checkRecord(final T scanRecord, final CheckRecordCallback callback) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final T record = mRecordDao.loadSync(scanRecord.getQrCode());
                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (record != null) {
                            callback.onRecordExisted(record);
                        } else {
                            callback.onNewRecord(scanRecord);
                        }
                    }
                });
            }
        };

        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public LiveData<Integer> getTotal() {
        return mRecordDao.count();
    }

    @Override
    public LiveData<Integer> getSent() {
        return mRecordDao.countSent();
    }

    @Override
    public void sync(final SyncCallback callback, final Class<T> cls) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                List<T> records = mRecordDao.loadNotSentSync();

                try {
                    for (T record : records) {

                        Response<IScanResponse<String>> response = null;

                        if (cls.equals(Visitor.class)) {
                            response = IScanClient.getApiService()
                                    .syncVisitors(IScanFieldMaps.syncVisitors((Visitor) record)).execute();
                        } else if (cls.equals(Booth.class)) {
                            response = IScanClient.getApiService()
                                    .syncVisitors(IScanFieldMaps.syncBooths((Booth) record)).execute();
                        }

                        if (response.body().getStatus().equals("200")) {
                            record.setSent(true);
                            mRecordDao.update(record);
                        }
                    }
                    mAppExecutors.mainThread().execute(new Runnable() {
                        @Override
                        public void run() {
                            callback.onSyncFinished(Status.SUCCESS);
                        }
                    });

                } catch (final Throwable e) {
                    mAppExecutors.mainThread().execute(new Runnable() {
                        @Override
                        public void run() {
                            if (e instanceof UnknownHostException) {
                                callback.onSyncFinished(Status.DISCONNECTED);
                            } else if (e instanceof ConnectException) {
                                callback.onSyncFinished(Status.NO_NETWORK);
                            } else if (e instanceof IllegalArgumentException) {
                                callback.onSyncFinished(Status.BAD_URL);
                            } else if (e instanceof NullPointerException) {
                                callback.onSyncFinished(Status.NOT_A_RECORD);
                            } else {
                                callback.onSyncFinished(Status.UNKNOWN);
                            }
                        }
                    });
                }
            }
        };

        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public void delete(final long id) {
        mAppExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mRecordDao.delete(id);
            }
        });
    }

    @Override
    public void insert(final T record) {
        record.setScannerId(Injector.get().userManager().getUser().getRegisterId());
        record.setTimestamp(currentTimeMillis());
        record.setSent(false);

        mAppExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mRecordDao.insert(record);
            }
        });
    }

    @Override
    public void update(final T record) {
        record.setTimestamp(currentTimeMillis());
        record.setSent(false);

        mAppExecutors.diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mRecordDao.update(record);
            }
        });
    }

    public void download(final AppDatabase db, final DownloadCallback callback) {
        final User user = Injector.get().userManager().getUser();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    final Response<IScanResponse<Action>> actionResponse = IScanClient.getApiService()
                            .getActions(IScanFieldMaps.actions()).execute();

                    mAppExecutors.diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            List<Action> actionList = actionResponse.body().getData();
                            Action[] actions = actionList.toArray(new Action[actionList.size()]);
                            db.actionDao().insert(actions);
                        }
                    });

                    if (user.getType().equals(UserType.EXHIBITOR)) {
                        final Response<IScanResponse<ExhibitorProduct>> exhibitorProductsResponse = IScanClient.getApiService()
                                .getExhibitorProducts(IScanFieldMaps.exhibitorProducts(user.getCompanyId())).execute();

                        mAppExecutors.diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                List<ExhibitorProduct> exhibitorProductList = exhibitorProductsResponse.body().getData();

                                for (ExhibitorProduct product : exhibitorProductList) {
                                    product.setCompanyId(user.getCompanyId());
                                }

                                ExhibitorProduct[] exhibitorProducts = exhibitorProductList
                                        .toArray(new ExhibitorProduct[exhibitorProductList.size()]);
                                db.exhibitorProductDao().insert(exhibitorProducts);
                            }
                        });
                    }

                    if (user.getType().equals(UserType.VISITOR)) {
                        final Response<IScanResponse<Product>> productResponse = IScanClient.getApiService()
                                .getProducts(IScanFieldMaps.products()).execute();

                        mAppExecutors.diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                List<Product> productList = productResponse.body().getData();
                                Product[] products = productList.toArray(new Product[productList.size()]);
                                db.productDao().insert(products);
                            }
                        });
                    }

                    Response<IScanResponse<Guide>> guideResponse = IScanClient.getApiService()
                            .getGuide(IScanFieldMaps.guide(user.getType())).execute();
                    List<Guide> guides = guideResponse.body().getData();
                    if (guides.size() > 0) {
                        Guide guide = guides.get(0);
                        db.saveGuide(guide);
                    }
                    mAppExecutors.mainThread().execute(new Runnable() {
                        @Override
                        public void run() {
                            callback.onDownloadComplete(Status.SUCCESS);
                        }
                    });

                } catch (final Throwable e) {
                    mAppExecutors.mainThread().execute(new Runnable() {
                        @Override
                        public void run() {
                            if (e instanceof UnknownHostException) {
                                callback.onDownloadComplete(Status.DISCONNECTED);
                            } else if (e instanceof ConnectException) {
                                callback.onDownloadComplete(Status.NO_NETWORK);
                            } else if (e instanceof IllegalArgumentException) {
                                callback.onDownloadComplete(Status.BAD_URL);
                            } else if (e instanceof NullPointerException) {
                                callback.onDownloadComplete(Status.NOT_A_RECORD);
                            } else {
                                callback.onDownloadComplete(Status.UNKNOWN);
                            }
                        }
                    });
                }
            }
        };

        mAppExecutors.networkIO().execute(runnable);
    }


}
