package com.corpit.iscan.data;

import android.arch.lifecycle.LiveData;
import android.support.annotation.IntDef;

import com.corpit.iscan.ui.base.IRecord;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public interface RecordsDataSource<T extends IRecord> {

    interface SyncCallback {
        void onSyncFinished(@Status int status);
    }

    interface DownloadCallback {
        void onDownloadComplete(@Status int status);
    }

    interface CheckRecordCallback {

        void onRecordExisted(IRecord record);

        void onNewRecord(IRecord record);
    }

    LiveData<List<T>> getRecords();

    void checkRecord(T record, CheckRecordCallback callback);

    LiveData<Integer> getTotal();

    LiveData<Integer> getSent();

    void sync(SyncCallback callback, Class<T> dataType);

    void delete(final long id);

    void insert(final T record);

    void update(final T record);

    @IntDef({Status.SUCCESS, Status.DISCONNECTED, Status.NO_NETWORK, Status.BAD_URL,
            Status.NOT_A_RECORD, Status.UNKNOWN})
    @Retention(RetentionPolicy.SOURCE)
    @interface Status {
        int SUCCESS = 1;
        int DISCONNECTED = 2;
        int NO_NETWORK = 3;
        int BAD_URL = 4;
        int NOT_A_RECORD = 5;
        int UNKNOWN = 6;
    }
}
