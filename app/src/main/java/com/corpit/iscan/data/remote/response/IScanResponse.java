package com.corpit.iscan.data.remote.response;

import com.squareup.moshi.Json;

import java.util.List;

public class IScanResponse<T> {

    @Json(name = "data")
    private List<T> data;

    @Json(name = "message")
    private String message;

    @Json(name = "Status")
    private String status;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
