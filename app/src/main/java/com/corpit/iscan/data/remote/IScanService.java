package com.corpit.iscan.data.remote;

import com.corpit.iscan.data.remote.response.IScanResponse;
import com.corpit.iscan.model.Action;
import com.corpit.iscan.model.ExhibitorProduct;
import com.corpit.iscan.model.Guide;
import com.corpit.iscan.model.Product;
import com.corpit.iscan.model.Show;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface IScanService {

    @FormUrlEncoded
    @POST("masterapi")
    Call<IScanResponse<Object>> login(@FieldMap Map<String, String> body);

    @FormUrlEncoded
    @POST("masterapi")
    Call<IScanResponse<Action>> getActions(@FieldMap Map<String, String> body);

    @FormUrlEncoded
    @POST("masterapi")
    Call<IScanResponse<Product>> getProducts(@FieldMap Map<String, String> body);

    @FormUrlEncoded
    @POST("masterapi")
    Call<IScanResponse<ExhibitorProduct>> getExhibitorProducts(@FieldMap Map<String, String> body);

    @FormUrlEncoded
    @POST("masterapi")
    Call<IScanResponse<Guide>> getGuide(@FieldMap Map<String, String> body);

    @FormUrlEncoded
    @POST("masterapi")
    Call<IScanResponse<String>> syncVisitors(@FieldMap Map<String, String> body);

    @FormUrlEncoded
    @POST("masterapi")
    Call<IScanResponse<String>> syncBooths(@FieldMap Map<String, String> body);

    @FormUrlEncoded
    @POST("masterapi")
    Call<IScanResponse<Show>> getShows(@FieldMap Map<String, String> body);

}
