package com.corpit.iscan.data.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.content.SharedPreferences;

import com.corpit.iscan.data.db.dao.ActionDao;
import com.corpit.iscan.data.db.dao.BoothDao;
import com.corpit.iscan.data.db.dao.ExhibitorProductDao;
import com.corpit.iscan.data.db.dao.ProductDao;
import com.corpit.iscan.data.db.dao.VisitorDao;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.model.Action;
import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.ExhibitorProduct;
import com.corpit.iscan.model.Guide;
import com.corpit.iscan.model.Product;
import com.corpit.iscan.model.Visitor;

@Database(
        entities = {
                Visitor.class,
                Booth.class,
                Product.class,
                Action.class,
                ExhibitorProduct.class},
        version = 1,
        exportSchema = false
)
public abstract class AppDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "Standard2018v1.db";

    private static String KEY_GUIDE;

    private static AppDatabase sInstance;

    private static SharedPreferences sharedPref;

    public abstract VisitorDao visitorDao();

    public abstract BoothDao boothDao();

    public abstract ActionDao actionDao();

    public abstract ExhibitorProductDao exhibitorProductDao();

    public abstract ProductDao productDao();

    public static AppDatabase initDatabase(Context context) {
        String dbName = Injector.get().userManager().getShowId() + ".db";
        sInstance = Room.databaseBuilder(context.getApplicationContext(),
                AppDatabase.class, dbName).build();

        KEY_GUIDE = Injector.get().userManager().getShowId() + "guide";
        sharedPref = context.getApplicationContext()
                .getSharedPreferences(KEY_GUIDE, Context.MODE_PRIVATE);

        return sInstance;
    }

    public static AppDatabase getInstance(final Context context) {
        if (sInstance == null) {
            synchronized (AppDatabase.class) {
                if (sInstance == null) {
                    initDatabase(context);
                }
            }
        }
        return sInstance;
    }

    public void saveGuide(Guide guide) {
        sharedPref.edit().clear().apply();
        sharedPref.edit().putString(KEY_GUIDE, guide.getHtmlString()).apply();
    }

    public String getGuide() {
        return sharedPref.getString(KEY_GUIDE, "");
    }

}
