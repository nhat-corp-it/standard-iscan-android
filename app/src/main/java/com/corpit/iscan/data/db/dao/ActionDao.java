package com.corpit.iscan.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.corpit.iscan.model.Action;

import java.util.List;

@Dao
public interface ActionDao {

    @Query("SELECT name FROM `Action`")
    List<String> loadAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Action... actions);
}
