package com.corpit.iscan.data;

import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.data.db.dao.ProductDao;
import com.corpit.iscan.model.Product;

import javax.inject.Inject;

public class ProductRepository {

    private final ProductDao mProductDao;

    @Inject
    public ProductRepository(AppDatabase database) {
        mProductDao = database.productDao();
    }

    public Product loadById(String id) {
        return mProductDao.loadById(id);
    }
}
