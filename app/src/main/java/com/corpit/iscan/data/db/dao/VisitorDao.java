package com.corpit.iscan.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.corpit.iscan.model.Visitor;

import java.util.List;

@Dao
public interface VisitorDao extends RecordDao<Visitor>{

    @Query("SELECT * FROM visitor")
    @Override
    LiveData<List<Visitor>> loadAll();

    @Query("SELECT * FROM visitor WHERE sent = 0")
    @Override
    List<Visitor> loadNotSentSync();

    @Query("SELECT * FROM visitor WHERE qr_code = :qrCode LIMIT 1")
    @Override
    Visitor loadSync(String qrCode);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @Override
    void insert(Visitor visitors);

    @Query("DELETE FROM visitor WHERE id = :id")
    @Override
    void delete(long id);

    @Update
    @Override
    void update(Visitor visitor);

    @Query("SELECT COUNT(*) FROM visitor WHERE sent = 1")
    @Override
    LiveData<Integer> countSent();

    @Query("SELECT COUNT(*) FROM visitor")
    @Override
    LiveData<Integer> count();
}
