package com.corpit.iscan.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Update;

import com.corpit.iscan.ui.base.IRecord;

import java.util.List;

public interface RecordDao<T extends IRecord> {

    LiveData<List<T>> loadAll();

    List<T> loadNotSentSync();

    T loadSync(String qrCode);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(T record);

    void delete(long id);

    @Update
    void update(T record);

    LiveData<Integer> countSent();

    LiveData<Integer> count();
}
