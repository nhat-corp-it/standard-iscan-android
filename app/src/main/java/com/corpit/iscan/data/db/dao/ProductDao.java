package com.corpit.iscan.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.corpit.iscan.model.Product;

import java.util.List;

@Dao
public interface ProductDao {

    @Query("SELECT * FROM product")
    List<Product> loadAll();

    @Query("SELECT * FROM product WHERE id = :id LIMIT 1")
    Product loadById(String id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Product... products);

}
