package com.corpit.iscan.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.corpit.iscan.model.ExhibitorProduct;

import java.util.List;

@Dao
public interface ExhibitorProductDao {

    @Query("SELECT product_name FROM exhibitorproduct WHERE company_id = :companyId")
    List<String> loadAll(String companyId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ExhibitorProduct... exhibitorProducts);

}
