package com.corpit.iscan.data.db.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.corpit.iscan.model.Booth;

import java.util.List;

@Dao
public interface BoothDao extends RecordDao<Booth> {

    @Query("SELECT * FROM booth")
    @Override
    LiveData<List<Booth>> loadAll();

    @Query("SELECT * FROM booth WHERE sent = 0")
    @Override
    List<Booth> loadNotSentSync();

    @Query("SELECT * FROM booth WHERE qr_code = :qrCode LIMIT 1")
    @Override
    Booth loadSync(String qrCode);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @Override
    void insert(Booth booth);

    @Query("DELETE FROM booth WHERE id = :id")
    @Override
    void delete(long id);

    @Update
    @Override
    void update(Booth booth);

    @Query("SELECT COUNT(*) FROM booth WHERE sent = 1")
    @Override
    LiveData<Integer> countSent();

    @Query("SELECT COUNT(*) FROM booth")
    @Override
    LiveData<Integer> count();
}
