package com.corpit.iscan.sync;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.data.db.dao.BoothDao;
import com.corpit.iscan.data.db.dao.VisitorDao;
import com.corpit.iscan.data.remote.IScanClient;
import com.corpit.iscan.data.remote.IScanFieldMaps;
import com.corpit.iscan.data.remote.response.IScanResponse;
import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.Visitor;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkStatus;
import androidx.work.Worker;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class SyncWorker extends Worker {

    private static final String NAME_SYNC_PREF = "SyncPref";
    private static final String KEY_WORKER_ID = "WorkerId";

    @NonNull
    @Override
    public Result doWork() {

        sync();

        return Result.SUCCESS;
    }

    private void sync() {
        AppDatabase db = AppDatabase.getInstance(getApplicationContext());
        VisitorDao visitorDao = db.visitorDao();

        List<Visitor> visitors = visitorDao.loadNotSentSync();

        for (Visitor visitor : visitors) {
            try {
                Response<IScanResponse<String>> response = IScanClient.getApiService()
                        .syncVisitors(IScanFieldMaps.syncVisitors(visitor)).execute();

                if (response.body() != null && response.body().getStatus().equals("200")) {
                    visitor.setSent(true);
                    visitorDao.update(visitor);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        BoothDao boothDao = db.boothDao();

        List<Booth> booths = boothDao.loadNotSentSync();

        for (Booth booth : booths) {
            try {
                Response<IScanResponse<String>> response = IScanClient.getApiService()
                        .syncBooths(IScanFieldMaps.syncBooths(booth)).execute();

                if (response.body() != null && response.body().getStatus().equals("200")) {
                    booth.setSent(true);
                    boothDao.update(booth);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        SharedPreferences preferences = getApplicationContext()
                .getSharedPreferences(NAME_SYNC_PREF, MODE_PRIVATE);
        preferences.edit().putString(KEY_WORKER_ID, "").apply();
    }

    public static void attemptToSync(Context context) {
        SharedPreferences preferences = context.getApplicationContext()
                .getSharedPreferences(NAME_SYNC_PREF, MODE_PRIVATE);
        String workerId = preferences.getString(KEY_WORKER_ID, "");

        WorkStatus status = null;
        if (!TextUtils.isEmpty(workerId)) {
            status = WorkManager.getInstance().getStatusById(UUID.fromString(workerId)).getValue();
        }


        if (status == null || status.getState().isFinished()) {
            Constraints constraints = new Constraints.Builder()
                    .setRequiredNetworkType(NetworkType.UNMETERED)
                    .setRequiresBatteryNotLow(true)
                    .build();

            OneTimeWorkRequest request = new OneTimeWorkRequest.Builder(SyncWorker.class)
                    .setConstraints(constraints)
                    .build();

            WorkManager.getInstance().enqueue(request);

            preferences.edit().putString(KEY_WORKER_ID, request.getId().toString()).apply();
        }

    }
}
