package com.corpit.iscan.ui.base;

public interface OnItemClickListener<T extends IRecord> {

    void onClick(T record);
}
