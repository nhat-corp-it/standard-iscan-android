package com.corpit.iscan.ui.followup;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.corpit.iscan.R;
import com.corpit.iscan.model.Visitor;
import com.corpit.iscan.ui.base.BannerActivity;
import com.corpit.iscan.ui.history.VisitorActivity;
import com.corpit.iscan.util.AlertHelper;
import com.corpit.iscan.viewmodel.FollowUpViewModel;

import java.util.Arrays;
import java.util.List;

import static com.corpit.iscan.ui.history.VisitorActivity.KEY_VISITOR_DATA;

public class FollowUpActivity extends BannerActivity {

    private List<String> selectedProducts;

    private List<String> selectedActions;

    private RecyclerView productList;

    private RecyclerView actionList;
    private CheckboxAdapter productAdapter;
    private CheckboxAdapter actionAdapter;
    private Visitor visitor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_up);

        visitor = getIntent().getParcelableExtra(KEY_VISITOR_DATA);

        productList = findViewById(R.id.product_list);
        actionList = findViewById(R.id.action_list);

        String[] actions = new String[0];
        if (visitor.getActions() != null) {
            actions = visitor.getActions().split("\\^");
        }
        selectedActions = Arrays.asList(actions);

        String[] products = new String[0];
        if (visitor.getProducts() != null) {
            products = visitor.getProducts().split("\\^");
        }
        selectedProducts = Arrays.asList(products);

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (productAdapter != null && actionAdapter != null) {
                    visitor.setProducts(productAdapter.getSelected());
                    visitor.setActions(actionAdapter.getSelected());
                    Intent intent = new Intent();
                    intent.putExtra(KEY_VISITOR_DATA, visitor);
                    gotoActivity(VisitorActivity.class, intent, VisitorActivity.REQUEST_CODE);
                }
            }
        });

        FollowUpViewModel viewModel = ViewModelProviders.of(this).get(FollowUpViewModel.class);

        subscribeToModel(viewModel);
    }

    private void subscribeToModel(FollowUpViewModel viewModel) {
        viewModel.getProducts().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> products) {
                if (products != null) {
                    findViewById(R.id.product_section).setVisibility(View.VISIBLE);
                    productAdapter = new CheckboxAdapter(products, selectedProducts);
                    productList.setAdapter(productAdapter);
                }
            }
        });

        viewModel.getActions().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> actions) {
                if (actions != null) {
                    findViewById(R.id.action_section).setVisibility(View.VISIBLE);
                    actionAdapter = new CheckboxAdapter(actions, selectedActions);
                    actionList.setAdapter(actionAdapter);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VisitorActivity.REQUEST_CODE) {
            setResult(resultCode);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        AlertHelper.showGoBackDialog(this);
    }

}
