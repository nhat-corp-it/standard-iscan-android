package com.corpit.iscan.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.corpit.iscan.R;
import com.corpit.iscan.di.Injector;

public abstract class BannerActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Glide.with(this)
                .load(Injector.get().userManager().getShowBanner())
                .apply(new RequestOptions().placeholder(R.drawable.banner_holder))
                .into(((ImageView) findViewById(R.id.banner)));
    }
}
