package com.corpit.iscan.ui.shows;

public interface ClickCallback<T> {

    void onClick(T item);
}
