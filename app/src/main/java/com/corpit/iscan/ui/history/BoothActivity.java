package com.corpit.iscan.ui.history;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.corpit.iscan.R;
import com.corpit.iscan.data.ProductRepository;
import com.corpit.iscan.databinding.ActivityBoothBinding;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.Product;
import com.corpit.iscan.ui.base.BannerActivity;
import com.corpit.iscan.util.AlertHelper;
import com.corpit.iscan.viewmodel.RecordsViewModel;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BoothActivity extends BannerActivity {

    public static final int REQUEST_CODE = 3;

    public static final String KEY_BOOTH_DATA = "boothData";
    private ActivityBoothBinding binding;
    private Booth booth;

    public static final int NEW_BOOTH_RESULT_OK = RESULT_FIRST_USER + 6;
    public static final int EDIT_BOOTH_RESULT_OK = RESULT_FIRST_USER + 7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_booth);

        booth = getIntent().getParcelableExtra(KEY_BOOTH_DATA);

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                final ProductRepository productRepository = Injector.get().productRepo();
                StringBuilder builder = new StringBuilder();
                final String[] products = booth.getProducts().split("#");
                for (int i = 0; i < products.length; i++) {
                    final Product product = productRepository.loadById(products[i]);
                    builder.append(product.getName());
                    if (i < products.length - 1) builder.append("\n");
                }
                binding.boothProducts.setText(builder.toString());
            }
        });

        binding.setBooth(booth);

        ViewModelProvider.Factory factory = new RecordsViewModel.Factory(getApplication(), Booth.class);

        @SuppressWarnings("unchecked")
        final RecordsViewModel<Booth> viewModel = ViewModelProviders.of(this, factory).get(RecordsViewModel.class);

        binding.actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String remark = binding.boothRemark.getText().toString();
                booth.setRemark(remark);
                if (booth.getId() > 0) {
                    viewModel.onSaveButtonClick(booth);
                    setResult(EDIT_BOOTH_RESULT_OK);
                } else {
                    viewModel.insert(booth);
                    setResult(NEW_BOOTH_RESULT_OK);
                }
                finish();
            }
        });

        binding.backButton.setText(booth.getId() > 0 ? R.string.action_back : R.string.action_cancel);

        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (booth.getId() <= 0) {
            AlertHelper.showGoBackDialog(this);
        } else {
            super.onBackPressed();
        }
    }
}
