package com.corpit.iscan.ui.more;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;

import com.corpit.iscan.R;
import com.corpit.iscan.databinding.ActivityRecordsSumBinding;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.model.Visitor;
import com.corpit.iscan.ui.base.BannerActivity;
import com.corpit.iscan.ui.base.IRecord;
import com.corpit.iscan.util.NetworkUtil;
import com.corpit.iscan.viewmodel.RecordsViewModel;

public class RecordsSumActivity extends BannerActivity {

    private ProgressDialog progressDialog;
    private ActivityRecordsSumBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_records_sum);

        progressDialog = new ProgressDialog(this);

        final RecordsViewModel viewModel;

        ViewModelProvider.Factory factory = null;

        String userType = Injector.get().userManager().getType();
        switch (userType) {
            case UserType.EXHIBITOR:
                factory = new RecordsViewModel.Factory(getApplication(), Visitor.class);
                break;
            case UserType.VISITOR:
                factory = new RecordsViewModel.Factory(getApplication(), Booth.class);
                break;
        }

        viewModel = ViewModelProviders.of(this, factory).get(RecordsViewModel.class);
        subscribeUi(viewModel);

        mBinding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mBinding.syncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.isNetworkWorking(RecordsSumActivity.this)) {
                    viewModel.onSyncButtonClick();
                } else {
                    Snackbar.make(mBinding.view, R.string.error_no_network, Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private <T extends IRecord> void subscribeUi(final RecordsViewModel<T> viewModel) {
        viewModel.getTotal().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null) {
                    mBinding.setSize(integer);
                }
                // espresso does not know how to wait for data binding's loop so we execute changes
                // sync.
                mBinding.executePendingBindings();
            }
        });

        viewModel.getSent().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable Integer integer) {
                if (integer != null) {
                    mBinding.setSent(integer);
                }
                mBinding.executePendingBindings();
            }
        });

        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isLoading) {
                if (isLoading != null && isLoading) {
                    progressDialog.show();
                    progressDialog.setContentView(R.layout.progress_dialog);
                } else {
                    progressDialog.dismiss();
                }
            }
        });

        viewModel.getSnackbarMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String message) {
                if (!TextUtils.isEmpty(message)) {
                    Snackbar.make(mBinding.view, message, Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }
}
