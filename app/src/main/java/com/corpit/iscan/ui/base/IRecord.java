package com.corpit.iscan.ui.base;

import android.os.Parcelable;

public interface IRecord extends Parcelable{

    long getId();

    String getName();

    String getCompany();

    String getQrCode();

    void setScannerId(String scannerId);

    long getTimestamp();

    void setTimestamp(long timestamp);

    boolean isSent();

    void setSent(boolean sent);
}
