package com.corpit.iscan.ui.scan;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PointF;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

import com.corpit.iscan.R;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.User;
import com.corpit.iscan.model.Visitor;
import com.corpit.iscan.ui.base.BaseActivity;
import com.corpit.iscan.util.QrDecodeHelper.QrCodeType;
import com.dlazaro66.qrcodereaderview.QRCodeReaderView;

import static com.corpit.iscan.util.QrDecodeHelper.isValidQrCode;
import static com.corpit.iscan.util.QrDecodeHelper.newDecrypt;
import static com.corpit.iscan.util.QrDecodeHelper.toBooth;
import static com.corpit.iscan.util.QrDecodeHelper.toUser;
import static com.corpit.iscan.util.QrDecodeHelper.toVisitor;

public class ScanActivity extends BaseActivity implements QRCodeReaderView.OnQRCodeReadListener {

    public static final int REQUEST_CODE = 1;

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 101;

    public static final long VIBRATE_TIME = 200L;

    public static final String KEY_SCAN_TYPE = "scan_type";

    public static final String KEY_SCAN_DATA = "scan_data";

    public static final int SCAN_TYPE_USER_EXHIBITOR = RESULT_FIRST_USER + 1;
    public static final int SCAN_TYPE_USER_VISITOR = RESULT_FIRST_USER + 2;
    public static final int SCAN_TYPE_VISITOR = RESULT_FIRST_USER + 3;
    public static final int SCAN_TYPE_BOOTH = RESULT_FIRST_USER + 4;

    private int scanType;

    private QRCodeReaderView qrReader;
    private ImageView scanline;
    private Vibrator vibrator;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        scanType = getIntent().getIntExtra(KEY_SCAN_TYPE, -1);

        qrReader = findViewById(R.id.qr_reader);

        scanline = findViewById(R.id.scan_line);

        if (checkPermission()) {
            qrReader.setOnQRCodeReadListener(this);

            // Use this function to change the auto focus interval (default is 5 secs)
            qrReader.setAutofocusInterval(1000L);

            // Use this function to enable/disable Torch
            qrReader.setTorchEnabled(true);

            // Use this function to set back camera preview
            qrReader.setBackCamera();

            qrReader.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    qrReader.forceAutoFocus();
                    return false;
                }
            });
        }

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

    }

    @Override
    public void onStart() {
        super.onStart();
        TranslateAnimation animation =
                new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0.0f,
                        Animation.RELATIVE_TO_PARENT, 0.0f, Animation.RELATIVE_TO_PARENT,
                        0.0f, Animation.RELATIVE_TO_PARENT, 0.9f);
        animation.setDuration(2000);
        animation.setRepeatCount(-1);
        animation.setRepeatMode(Animation.RESTART);
        scanline.startAnimation(animation);
        qrReader.setQRDecodingEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        qrReader.startCamera();
        qrReader.setQRDecodingEnabled(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        qrReader.stopCamera();
        qrReader.setQRDecodingEnabled(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setResult(RESULT_CANCELED);
                    finish();
                } else {
                    Snackbar.make(findViewById(R.id.snackbar_placeholder),
                            R.string.error_camera_permission, Snackbar.LENGTH_SHORT).show();
                    setResult(RESULT_CANCELED);
                    finish();
                }
                break;
        }
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
            return false;
        }

        return true;
    }

    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        qrReader.stopCamera();
        qrReader.setQRDecodingEnabled(false);
        scanline.clearAnimation();
        vibrator.vibrate(VIBRATE_TIME);
        String decodedString;
        try {
            decodedString = newDecrypt(text, Injector.get().userManager().getShowEncKey());
        } catch (Exception e) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.error_invalid_qr)
                    .setPositiveButton(R.string.action_try_again, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            recreate();
                        }
                    })
                    .setNegativeButton(R.string.action_back, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    });
            builder.create().show();
            return;
        }

        Intent intent = new Intent();

        boolean isValid = false;
        switch (scanType) {
            case SCAN_TYPE_USER_EXHIBITOR:
                if (isValidQrCode(decodedString, QrCodeType.EXHIBITOR)) {
                    isValid = true;
                    User user = toUser(decodedString);
                    intent.putExtra(KEY_SCAN_DATA, user);
                }
                break;
            case SCAN_TYPE_USER_VISITOR:
                if (isValidQrCode(decodedString, QrCodeType.VISITOR)) {
                    isValid = true;
                    User user = toUser(decodedString);
                    intent.putExtra(KEY_SCAN_DATA, user);
                }
                break;
            case SCAN_TYPE_VISITOR:
                if (isValidQrCode(decodedString, QrCodeType.VISITOR)
                        || isValidQrCode(decodedString, QrCodeType.EXHIBITOR)) {
                    isValid = true;
                    Visitor visitor = toVisitor(decodedString);
                    intent.putExtra(KEY_SCAN_DATA, visitor);
                }
                break;
            case SCAN_TYPE_BOOTH:
                if (isValidQrCode(decodedString, QrCodeType.BOOTH)) {
                    isValid = true;
                    Booth booth = toBooth(decodedString);
                    intent.putExtra(KEY_SCAN_DATA, booth);
                }
                break;
        }

        if (isValid) {
            setResult(scanType, intent);
            finish();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.error_invalid_qr)
                    .setPositiveButton(R.string.action_try_again, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            recreate();
                        }
                    })
                    .setNegativeButton(R.string.action_back, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    });
            builder.create().show();
        }
    }
}
