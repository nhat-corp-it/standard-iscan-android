package com.corpit.iscan.ui.history;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.corpit.iscan.databinding.RowRecordBinding;
import com.corpit.iscan.ui.base.IRecord;
import com.corpit.iscan.ui.base.OnItemClickListener;

import java.util.List;

public class RecordAdapter extends RecyclerView.Adapter<RecordAdapter.RecordHolder> {

    private List<IRecord> mData;
    private OnItemClickListener<IRecord> mListener;

    RecordAdapter(OnItemClickListener<IRecord> listener) {
        this.mListener = listener;
    }

    @SuppressWarnings("unchecked")
    public <T extends IRecord> void setList(List<T> data) {
        DiffUtil.DiffResult result = DiffUtil.calculateDiff(new RecordDiffCallback(mData, (List<IRecord>) data));
        mData = (List<IRecord>) data;
        result.dispatchUpdatesTo(this);
    }

    public List<IRecord> getList() {
        return mData;
    }

    @NonNull
    @Override
    public RecordHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowRecordBinding binding = RowRecordBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new RecordHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecordAdapter.RecordHolder holder, int position) {
        final IRecord record = mData.get(position);

        holder.binding.setRecord(record);
        holder.binding.setListener(mListener);

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

//    public long getRecordId(int position) {
//        return mData.get(position).getId();
//    }

    class RecordHolder extends RecyclerView.ViewHolder {

        RowRecordBinding binding;

        RecordHolder(RowRecordBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    class RecordDiffCallback extends DiffUtil.Callback {

        List<IRecord> oldList;
        List<IRecord> newList;

        RecordDiffCallback(List<IRecord> oldList, List<IRecord> newList) {
            this.oldList = oldList;
            this.newList = newList;
        }

        @Override
        public int getOldListSize() {
            return oldList == null ? 0 : oldList.size();
        }

        @Override
        public int getNewListSize() {
            return newList == null ? 0 : newList.size();
        }

        @Override
        public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).getId() == newList.get(newItemPosition).getId();
        }

        @Override
        public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
            return oldList.get(oldItemPosition).equals(newList.get(newItemPosition));
        }
    }
}
