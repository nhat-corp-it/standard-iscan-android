package com.corpit.iscan.ui.more;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.corpit.iscan.R;
import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.ui.base.BannerActivity;

public class HowToUseActivity extends BannerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_how_to_use);

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String html = AppDatabase.getInstance(getApplicationContext()).getGuide();
        WebView webView = findViewById(R.id.content);
        webView.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);
    }
}
