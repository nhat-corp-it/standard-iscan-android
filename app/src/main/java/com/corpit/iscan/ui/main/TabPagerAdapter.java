package com.corpit.iscan.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.corpit.iscan.ui.history.HistoryFragment;
import com.corpit.iscan.ui.more.MoreFragment;
import com.corpit.iscan.ui.scan.ScanPrepareFragment;

import java.util.ArrayList;
import java.util.List;

public class TabPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragments = new ArrayList<>();

    TabPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments.add(new ScanPrepareFragment());
        fragments.add(new HistoryFragment());
        fragments.add(new MoreFragment());
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
