package com.corpit.iscan.ui.main;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.MenuItem;

import com.corpit.iscan.R;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.model.Visitor;
import com.corpit.iscan.ui.base.BannerActivity;
import com.corpit.iscan.ui.base.IRecord;
import com.corpit.iscan.ui.followup.FollowUpActivity;
import com.corpit.iscan.ui.history.BoothActivity;
import com.corpit.iscan.ui.history.VisitorActivity;
import com.corpit.iscan.ui.scan.ScanActivity;
import com.corpit.iscan.viewmodel.RecordsViewModel;

import static com.corpit.iscan.ui.history.BoothActivity.KEY_BOOTH_DATA;
import static com.corpit.iscan.ui.history.VisitorActivity.KEY_VISITOR_DATA;
import static com.corpit.iscan.ui.scan.ScanActivity.KEY_SCAN_DATA;
import static com.corpit.iscan.ui.scan.ScanActivity.SCAN_TYPE_BOOTH;
import static com.corpit.iscan.ui.scan.ScanActivity.SCAN_TYPE_VISITOR;

public class MainActivity extends BannerActivity implements MainActivityNavigator {

    private ViewPager viewPager;

    private BottomNavigationView navigationView;

    private MenuItem prevMenuItem;

    private TabPagerAdapter pagerAdapter;

    private RecordsViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewModelProvider.Factory factory = null;

        String userType = Injector.get().userManager().getType();
        switch (userType) {
            case UserType.EXHIBITOR:
                factory = new RecordsViewModel.Factory(getApplication(), Visitor.class);
                break;
            case UserType.VISITOR:
                factory = new RecordsViewModel.Factory(getApplication(), Booth.class);
                break;
        }

        viewModel = ViewModelProviders.of(this, factory).get(RecordsViewModel.class);

        subscribeToModel(this.viewModel);

        viewPager = findViewById(R.id.view_pager);

        navigationView = findViewById(R.id.bottom_navigation);

        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_scan:
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.navigation_history:
                        viewPager.setCurrentItem(1);
                        break;
                    case R.id.navigation_more:
                        viewPager.setCurrentItem(2);
                        break;
                }

                return false;
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    navigationView.getMenu().getItem(0).setChecked(false);
                }

                navigationView.getMenu().getItem(position).setChecked(true);
                prevMenuItem = navigationView.getMenu().getItem(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        pagerAdapter = new TabPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.beginFakeDrag();

    }

    private <T extends IRecord> void subscribeToModel(RecordsViewModel<T> viewModel) {
        viewModel.setMainNavigator(this);
        viewModel.getSnackbarMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String message) {
                if (!TextUtils.isEmpty(message)) {
                    Snackbar.make(findViewById(R.id.snackbar_placeholder), message, Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ScanActivity.REQUEST_CODE) {
            switch (resultCode) {
                case SCAN_TYPE_VISITOR:
                case SCAN_TYPE_BOOTH:
                    //noinspection unchecked
                    viewModel.checkRecord((IRecord) data.getParcelableExtra(KEY_SCAN_DATA));
                    break;
            }
        } else {
            viewModel.handleOnActivityResult(requestCode, resultCode);
        }

    }

    public Fragment getCurrentFragment() {
        return pagerAdapter.getItem(viewPager.getCurrentItem());
    }

    @Override
    public void showPage(int pageOrder) {
        viewPager.setCurrentItem(pageOrder);
    }

    @Override
    public void showUpdateDialog(final IRecord record, Class cls) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        if (cls.equals(Visitor.class)) {
            builder.setTitle(R.string.title_visitor_existed);
        } else if (cls.equals(Booth.class)) {
            builder.setTitle(R.string.title_booth_existed);
        }
        builder.setMessage(R.string.msg_existed)
                .setPositiveButton(R.string.action_update, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        viewModel.onUpdateButtonClick(record);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.action_cancel, null);

        builder.create().show();
    }

    @Override
    public <T extends IRecord> void gotoEditActivity(IRecord record, Class<T> cls) {
        Intent intent = new Intent();
        if (cls.equals(Visitor.class)) {
            intent.putExtra(KEY_VISITOR_DATA, record);
            gotoActivity(FollowUpActivity.class, intent, VisitorActivity.REQUEST_CODE);
        } else if (cls.equals(Booth.class)) {
            intent.putExtra(KEY_BOOTH_DATA, record);
            gotoActivity(BoothActivity.class, intent, BoothActivity.REQUEST_CODE);
        }

    }

    public RecordsViewModel getViewModel() {
        return viewModel;
    }
}
