package com.corpit.iscan.ui.followup;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.corpit.iscan.R;

import java.util.ArrayList;
import java.util.List;

public class CheckboxAdapter extends RecyclerView.Adapter<CheckboxAdapter.CheckboxHolder> {

    private List<CheckModel> items;

    CheckboxAdapter(List<String> names, List<String> selected) {
        items = new ArrayList<>();
        for (String name : names) {
            CheckModel checkModel = new CheckModel(name);
            if (selected.contains(name)) checkModel.setChecked(true);
            items.add(checkModel);
        }
    }

    public String getSelected() {
        int count = 0;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).isChecked) {
                count++;
                builder.append(items.get(i).name)
                        .append("^");
            }
        }
        if (count > 1) builder.deleteCharAt(builder.length() - 1);

        return builder.toString();
    }

    @NonNull
    @Override
    public CheckboxHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_checkbox, parent, false);
        return new CheckboxHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CheckboxHolder holder, int position) {
        holder.textView.setText(items.get(holder.getAdapterPosition()).getName());

        holder.checkBox.setOnCheckedChangeListener(null);

        holder.checkBox.setChecked(items.get(holder.getAdapterPosition()).isChecked);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                items.get(holder.getAdapterPosition()).setChecked(isChecked);
            }
        });

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.checkBox.setChecked(!holder.checkBox.isChecked());
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class CheckboxHolder extends RecyclerView.ViewHolder {

        CheckBox checkBox;
        TextView textView;
        View mView;

        CheckboxHolder(View view) {
            super(view);
            mView = view;
            checkBox = view.findViewById(R.id.checkbox);
            textView = view.findViewById(R.id.text);
        }
    }

    class CheckModel {
        String name;
        boolean isChecked = false;

        CheckModel(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isChecked() {
            return isChecked;
        }

        public void setChecked(boolean checked) {
            isChecked = checked;
        }
    }
}
