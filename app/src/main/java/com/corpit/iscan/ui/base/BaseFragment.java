package com.corpit.iscan.ui.base;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.corpit.iscan.R;
import com.corpit.iscan.ui.main.MainActivity;

public abstract class BaseFragment extends Fragment {

    @SuppressWarnings("ConstantConditions")
    protected void gotoFragment(Fragment fragment) {
        Fragment currentFragment;
        FragmentManager fragmentManager;
        if (getActivity() instanceof MainActivity) {
            currentFragment = ((MainActivity) getActivity()).getCurrentFragment();
            fragmentManager = currentFragment.getChildFragmentManager();
        } else {
            fragmentManager = getActivity().getSupportFragmentManager();
            currentFragment = fragmentManager.findFragmentById(R.id.root);
        }

        if (!fragment.getClass().toString().equals(currentFragment.getTag())) {
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.anim.push_left_in, R.anim.push_left_out,
                            R.anim.push_right_in, R.anim.push_right_out)
                    .replace(R.id.root, fragment, fragment.getClass().toString())
                    .addToBackStack(currentFragment.getClass().toString())
                    .commit();
        }
    }

    protected <T extends BaseActivity> void gotoActivity(Class<T> cls, @Nullable Intent intent, int requestCode) {
        if (getContext() == null) return;

        if (intent == null) {
            intent = new Intent();
        }
        intent.setClass(getContext(), cls);
        Bundle translateBundle = ActivityOptions.makeCustomAnimation(getContext(),
                R.anim.slide_in_left, R.anim.slide_out_left).toBundle();
        startActivityForResult(intent, requestCode, translateBundle);
    }
}