package com.corpit.iscan.ui.history;


import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.iscan.databinding.FragmentHistoryBinding;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.ui.base.BaseActivity;
import com.corpit.iscan.ui.base.BaseFragment;
import com.corpit.iscan.ui.base.IRecord;
import com.corpit.iscan.ui.base.OnItemClickListener;
import com.corpit.iscan.ui.main.MainActivity;
import com.corpit.iscan.viewmodel.RecordsViewModel;

import java.util.List;

import static com.corpit.iscan.ui.history.BoothActivity.KEY_BOOTH_DATA;
import static com.corpit.iscan.ui.history.VisitorActivity.KEY_VISITOR_DATA;

public class HistoryFragment extends BaseFragment implements OnItemClickListener<IRecord> {

    private FragmentHistoryBinding mBinding;

    private RecordAdapter mRecordAdapter;

    private RecyclerView.SmoothScroller smoothScroller;
    private RecordsViewModel viewModel;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = FragmentHistoryBinding.inflate(inflater, container, false);

        mRecordAdapter = new RecordAdapter(this);
        mBinding.recordList.setAdapter(mRecordAdapter);

        smoothScroller = new LinearSmoothScroller(container.getContext()) {
            @Override
            protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_END;
            }
        };

        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() == null) return;

        viewModel = ((MainActivity) getActivity()).getViewModel();
        subscribeUi(viewModel);

//        ItemTouchHelper touchHelper = new ItemTouchHelper(
//                new ItemTouchHelper.SimpleCallback(0,
//                        ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
//                    @Override
//                    public boolean onMove(RecyclerView recyclerView,
//                                          RecyclerView.ViewHolder viewHolder,
//                                          RecyclerView.ViewHolder target) {
//                        return false;
//                    }
//
//                    @Override
//                    public void onSwiped(RecyclerView.ViewHolder viewHolder,
//                                         int direction) {
//                        int position = viewHolder.getAdapterPosition();
//                        long id = mRecordAdapter.getRecordId(position);
//
//                        viewModel.delete(id);
//                    }
//                });

//        touchHelper.attachToRecyclerView(mBinding.recordList);
    }

    private <T extends IRecord> void subscribeUi(RecordsViewModel<T> viewModel) {
        // Update the list when the data changes
        viewModel.getData().observe(this, new RecordObserver<T>());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel.getData().removeObservers(this);
    }

    @Override
    public void onClick(IRecord record) {
        if (getActivity() == null) return;

        Intent intent = new Intent();

        String userType = Injector.get().userManager().getType();
        switch (userType) {
            case UserType.EXHIBITOR:
                intent.putExtra(KEY_VISITOR_DATA, record);
                ((BaseActivity) getActivity()).gotoActivity(VisitorActivity.class, intent, VisitorActivity.REQUEST_CODE);
                break;
            case UserType.VISITOR:
                intent.putExtra(KEY_BOOTH_DATA, record);
                ((BaseActivity) getActivity()).gotoActivity(BoothActivity.class, intent, BoothActivity.REQUEST_CODE);
                break;
        }
    }

    class RecordObserver<T extends IRecord> implements Observer<List<T>> {

        @Override
        public void onChanged(@Nullable List<T> items) {
            if (items != null) {
                mBinding.setIsLoading(false);
                int currentCount = mRecordAdapter.getItemCount();
                mRecordAdapter.setList(items);

                if (currentCount < items.size()) {
                    smoothScroller.setTargetPosition(items.size() - 1);
                    mBinding.recordList.getLayoutManager().startSmoothScroll(smoothScroller);
                }
            } else {
                mBinding.setIsLoading(true);
            }
            // espresso does not know how to wait for data binding's loop so we execute changes
            // sync.
            mBinding.executePendingBindings();
        }
    }
}
