package com.corpit.iscan.ui.scan;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.iscan.R;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.ui.base.BaseActivity;
import com.corpit.iscan.ui.base.BaseFragment;

import static com.corpit.iscan.ui.scan.ScanActivity.KEY_SCAN_TYPE;
import static com.corpit.iscan.ui.scan.ScanActivity.SCAN_TYPE_BOOTH;
import static com.corpit.iscan.ui.scan.ScanActivity.SCAN_TYPE_VISITOR;

public class ScanPrepareFragment extends BaseFragment {

    public ScanPrepareFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_scan_prepare, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.scan_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() == null) return;

                String userType = Injector.get().userManager().getType();
                Intent intent = new Intent();
                switch (userType) {
                    case UserType.EXHIBITOR:
                        intent.putExtra(KEY_SCAN_TYPE, SCAN_TYPE_VISITOR);
                        ((BaseActivity) getActivity()).gotoActivity(ScanActivity.class, intent, ScanActivity.REQUEST_CODE);
                        break;
                    case UserType.VISITOR:
                        intent.putExtra(KEY_SCAN_TYPE, SCAN_TYPE_BOOTH);
                        ((BaseActivity) getActivity()).gotoActivity(ScanActivity.class, intent, ScanActivity.REQUEST_CODE);
                        break;
                }
            }
        });
    }
}
