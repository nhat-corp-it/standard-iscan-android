package com.corpit.iscan.ui.main;

import com.corpit.iscan.ui.base.IRecord;

public interface MainActivityNavigator {

    void showPage(int pageOrder);

    void showUpdateDialog(IRecord record, Class cls);

    <T extends IRecord> void gotoEditActivity(IRecord record, Class<T> cls);
}
