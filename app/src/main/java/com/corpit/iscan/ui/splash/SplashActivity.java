package com.corpit.iscan.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.corpit.iscan.R;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.ui.main.MainActivity;
import com.corpit.iscan.ui.shows.ShowsActivity;

import static com.corpit.iscan.sync.SyncWorker.attemptToSync;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //Immersive mode
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);

        if (Injector.get().userManager().isLogin()) {
            attemptToSync(getApplicationContext());
        }

        splash();
    }

    private void splash() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent intent;
                if (Injector.get().userManager().isLogin()) {
                    intent = new Intent(SplashActivity.this, MainActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, ShowsActivity.class);
                }
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

}
