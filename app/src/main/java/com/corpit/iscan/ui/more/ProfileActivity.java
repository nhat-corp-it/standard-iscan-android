package com.corpit.iscan.ui.more;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.corpit.iscan.R;
import com.corpit.iscan.databinding.ActivityProfileBinding;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.ui.base.BannerActivity;
import com.corpit.iscan.util.AlertHelper;

public class ProfileActivity extends BannerActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityProfileBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);

        binding.setUser(Injector.get().userManager().getUser());

        binding.logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertHelper.showLogoutDialog(ProfileActivity.this);
            }
        });

        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
