package com.corpit.iscan.ui.history;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.corpit.iscan.R;
import com.corpit.iscan.databinding.ActivityVisitorBinding;
import com.corpit.iscan.model.Visitor;
import com.corpit.iscan.ui.base.BannerActivity;
import com.corpit.iscan.viewmodel.RecordsViewModel;

public class VisitorActivity extends BannerActivity {

    public static final int REQUEST_CODE = 2;

    public static final String KEY_VISITOR_DATA = "visitorData";

    public static final int NEW_VISITOR_RESULT_OK = RESULT_FIRST_USER + 4;
    public static final int EDIT_VISITOR_RESULT_OK = RESULT_FIRST_USER + 5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActivityVisitorBinding binding =
                DataBindingUtil.setContentView(this, R.layout.activity_visitor);

        final Visitor visitor = getIntent().getParcelableExtra(KEY_VISITOR_DATA);

        binding.setVisitor(visitor);

        ViewModelProvider.Factory factory = new RecordsViewModel.Factory(getApplication(), Visitor.class);
        @SuppressWarnings("unchecked")
        final RecordsViewModel<Visitor> viewModel = ViewModelProviders.of(this, factory).get(RecordsViewModel.class);

        binding.actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String remark = binding.visitorRemark.getText().toString();
                visitor.setRemark(remark);
                if (visitor.getId() > 0) {
                    viewModel.onSaveButtonClick(visitor);
                    setResult(EDIT_VISITOR_RESULT_OK);
                } else {
                    viewModel.insert(visitor);
                    setResult(NEW_VISITOR_RESULT_OK);
                }
                finish();
            }
        });

        binding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
