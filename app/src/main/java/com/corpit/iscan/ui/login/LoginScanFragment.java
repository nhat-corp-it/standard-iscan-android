package com.corpit.iscan.ui.login;


import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.corpit.iscan.R;
import com.corpit.iscan.model.User;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.ui.base.BaseFragment;
import com.corpit.iscan.ui.main.MainActivity;
import com.corpit.iscan.ui.scan.ScanActivity;
import com.corpit.iscan.viewmodel.LoginViewModel;

import static com.corpit.iscan.ui.scan.ScanActivity.KEY_SCAN_DATA;
import static com.corpit.iscan.ui.scan.ScanActivity.KEY_SCAN_TYPE;
import static com.corpit.iscan.ui.scan.ScanActivity.REQUEST_CODE;
import static com.corpit.iscan.ui.scan.ScanActivity.SCAN_TYPE_USER_EXHIBITOR;
import static com.corpit.iscan.ui.scan.ScanActivity.SCAN_TYPE_USER_VISITOR;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginScanFragment extends BaseFragment implements LoginNavigator {

    private static final String ARG_PARAM1 = "param1";

    @UserType
    private String loginFor;

    private ImageButton scanButton;

    private ProgressDialog progressDialog;

    private LoginViewModel viewModel;

    public LoginScanFragment() {
        // Required empty public constructor
    }

    public static LoginScanFragment newInstance(String loginFor) {
        LoginScanFragment fragment = new LoginScanFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, loginFor);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            loginFor = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login_scan, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final int scanType;
        String title = "";
        switch (loginFor) {
            case UserType.EXHIBITOR:
                scanType = SCAN_TYPE_USER_EXHIBITOR;
                title = "Exhibitor Login";
                break;
            case UserType.VISITOR:
                scanType = SCAN_TYPE_USER_VISITOR;
                title = "Visitor Login";
                break;
            default:
                scanType = -1;
                break;
        }

        ((TextView) view.findViewById(R.id.title)).setText(title);

        view.findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
            }
        });

        progressDialog = new ProgressDialog(getContext());

        scanButton = view.findViewById(R.id.scan_button);

        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(KEY_SCAN_TYPE, scanType);
                gotoActivity(ScanActivity.class, intent, ScanActivity.REQUEST_CODE);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() == null) return;

        viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);

        subscribeUi();
    }

    private void subscribeUi() {
        viewModel.setNavigator(this);

        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isLoading) {
                if (isLoading != null && isLoading) {
                    progressDialog.show();
                    progressDialog.setContentView(R.layout.progress_dialog);
                } else {
                    progressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE &&
                resultCode == SCAN_TYPE_USER_EXHIBITOR || resultCode == SCAN_TYPE_USER_VISITOR) {
            User user = data.getParcelableExtra(KEY_SCAN_DATA);

            viewModel.login(user, ((LoginActivity) getActivity()).getShow());
        }
    }

    @Override
    public void showLoginFailedDialog(String message) {
        if (getContext() == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.title_login_failed)
                .setMessage(message)
                .setPositiveButton(R.string.action_try_again, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        scanButton.callOnClick();
                    }
                })
                .setNegativeButton(R.string.action_cancel, null);
        builder.create().show();
    }

    @Override
    public void gotoMainActivity() {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), MainActivity.class);
            Bundle translateBundle = ActivityOptions.makeCustomAnimation(getContext(),
                    R.anim.slide_in_left, R.anim.slide_out_left).toBundle();
            startActivity(intent, translateBundle);
            ActivityCompat.finishAffinity(getActivity());
        }
    }

    @Override
    public void showDownloadFailedSnackBar(String errorMessage) {
        if (getView() != null) {
            Snackbar.make(getView(), errorMessage, Snackbar.LENGTH_SHORT).show();
        }
    }
}
