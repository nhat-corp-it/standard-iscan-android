package com.corpit.iscan.ui.login;

public interface LoginNavigator {

    void showLoginFailedDialog(String message);

    void gotoMainActivity();

    void showDownloadFailedSnackBar(String message);
}
