package com.corpit.iscan.ui.base;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.corpit.iscan.model.Show;
import com.corpit.iscan.util.DateConverter;

public class BindAdapters {

    @BindingAdapter("displayTimestamp")
    public static void displayTimestamp(TextView textView, long timestamp) {
        textView.setText(DateConverter.toText(timestamp, DateConverter.CLIENT_DATE_FORMAT));
    }

    @BindingAdapter("displayItems")
    public static void displayItems(TextView textView, String items) {
        items = items.replace("^", "\n");
        textView.setText(items);
    }

    @BindingAdapter("logoUrl")
    public static void logoUrl(ImageView imageView, String url) {
        Glide.with(imageView)
                .load(url)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(imageView);
    }

    @BindingAdapter("displayShowDate")
    public static void displayShowDate(TextView textView, Show show) {
        textView.setText(DateConverter.toText(show));
    }
}
