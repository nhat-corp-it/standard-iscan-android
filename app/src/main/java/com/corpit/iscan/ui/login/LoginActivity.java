package com.corpit.iscan.ui.login;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.corpit.iscan.R;
import com.corpit.iscan.model.Show;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.ui.base.BaseActivity;
import com.corpit.iscan.ui.base.BaseFragment;

import static com.corpit.iscan.ui.shows.ShowsActivity.KEY_SHOW;

public class LoginActivity extends BaseActivity {

    private Show show;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        show = getIntent().getParcelableExtra(KEY_SHOW);
        Glide.with(this)
                .load(show.getShowLogo())
                .into(((ImageView) findViewById(R.id.banner)));

        boolean visitorLogin = Boolean.parseBoolean(show.getVisitorLogin().toLowerCase());

        if (savedInstanceState == null) {
            BaseFragment fragment;
            if (visitorLogin) {
                fragment = new LoginFragment();
            } else {
                fragment = LoginScanFragment.newInstance(UserType.EXHIBITOR);
            }
            getSupportFragmentManager()
                    .beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .replace(R.id.root, fragment)
                    .commit();
        }

    }

    public Show getShow() {
        return show;
    }
}
