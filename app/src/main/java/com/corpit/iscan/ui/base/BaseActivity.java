package com.corpit.iscan.ui.base;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.corpit.iscan.R;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    public <T extends Activity> void gotoActivity(Class<T> cls) {
        gotoActivity(cls, null);
    }

    public <T extends Activity> void gotoActivity(Class<T> cls, boolean shouldFinish) {
        gotoActivity(cls);
        if (shouldFinish) finish();
    }

    public <T extends Activity> void gotoActivity(Class<T> cls, @Nullable Intent intent) {
        gotoActivity(cls, intent, -1);
    }

    public <T extends Activity> void gotoActivity(Class<T> cls, @Nullable Intent intent, int requestCode) {
        if (intent == null) intent = new Intent();
        intent.setClass(this, cls);
        Bundle translateBundle = ActivityOptions.makeCustomAnimation(this,
                R.anim.slide_in_left, R.anim.slide_out_left).toBundle();

        if(requestCode > 0){
            ActivityCompat.startActivityForResult(this, intent, requestCode, translateBundle);
        } else {
            startActivity(intent, translateBundle);
        }
    }
}
