package com.corpit.iscan.ui.shows;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.corpit.iscan.R;
import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.data.remote.IScanClient;
import com.corpit.iscan.data.remote.IScanFieldMaps;
import com.corpit.iscan.data.remote.response.IScanResponse;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.model.Show;
import com.corpit.iscan.ui.base.BaseActivity;
import com.corpit.iscan.ui.login.LoginActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowsActivity extends BaseActivity implements ClickCallback<Show> {

    public static final String KEY_SHOW = "show";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shows);

        fetchList();
    }

    private void fetchList() {
        Call<IScanResponse<Show>> call = IScanClient.getApiService().getShows(IScanFieldMaps.shows());
        call.enqueue(new Callback<IScanResponse<Show>>() {
            @Override
            public void onResponse(@NonNull Call<IScanResponse<Show>> call, @NonNull Response<IScanResponse<Show>> response) {
                findViewById(R.id.group).setVisibility(View.GONE);
                if (response.code() == 200 && response.body() != null) {
                    if (response.body().getStatus().equals("200")) {
                        populateList(response.body().getData());
                    } else {
                        Toast.makeText(ShowsActivity.this, "Failed to get list", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<IScanResponse<Show>> call, @NonNull Throwable t) {
                findViewById(R.id.group).setVisibility(View.VISIBLE);
                findViewById(R.id.retry_button).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        fetchList();
                    }
                });
                Toast.makeText(ShowsActivity.this, "Failed to get list", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void populateList(List<Show> data) {
        RecyclerView showList = findViewById(R.id.show_list);
        ShowAdapter adapter = new ShowAdapter(data, this);
        showList.setAdapter(adapter);
    }

    @Override
    public void onClick(Show show) {
        Intent intent = new Intent();
        intent.putExtra(KEY_SHOW, show);
        Injector.get().userManager().setShowId((show.getShowID()));
        Injector.get().userManager().setShowEncKey(show.getShowEncryptedKey());
        AppDatabase.initDatabase(this);
        gotoActivity(LoginActivity.class, intent);
    }
}
