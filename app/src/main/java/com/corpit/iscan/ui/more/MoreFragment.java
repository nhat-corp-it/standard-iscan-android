package com.corpit.iscan.ui.more;


import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.corpit.iscan.R;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.ui.base.BaseActivity;
import com.corpit.iscan.ui.base.BaseFragment;
import com.corpit.iscan.ui.base.IRecord;
import com.corpit.iscan.ui.main.MainActivity;
import com.corpit.iscan.ui.shows.ShowsActivity;
import com.corpit.iscan.viewmodel.RecordsViewModel;

import static android.app.Activity.RESULT_OK;

public class MoreFragment extends BaseFragment {

    private static final int REQUEST_LOG_OUT = 400;
    private ProgressDialog progressDialog;

    public MoreFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_more, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.records_sum_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((BaseActivity) getActivity()).gotoActivity(RecordsSumActivity.class);
            }
        });

        view.findViewById(R.id.how_to_use_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null)
                    ((BaseActivity) getActivity()).gotoActivity(HowToUseActivity.class);
            }
        });

        view.findViewById(R.id.profile_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoActivity(ProfileActivity.class, null, REQUEST_LOG_OUT);
            }
        });

        progressDialog = new ProgressDialog(getContext());

        if (getActivity() == null) return;

        final RecordsViewModel viewModel = ((MainActivity) getActivity()).getViewModel();

        view.findViewById(R.id.refresh_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.onRefreshButtonClick();
            }
        });

        subscribeToModel(viewModel);
    }

    private <T extends IRecord> void subscribeToModel(RecordsViewModel<T> viewModel) {
        viewModel.getSnackbarMessage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String message) {
                if (!TextUtils.isEmpty(message) && getActivity() != null) {
                    Snackbar.make(getActivity().findViewById(R.id.snackbar_placeholder),
                            message,
                            Snackbar.LENGTH_SHORT)
                            .show();
                }
            }
        });

        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isLoading) {
                if (isLoading != null && isLoading) {
                    progressDialog.show();
                    progressDialog.setContentView(R.layout.progress_dialog);
                } else {
                    progressDialog.dismiss();
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOG_OUT && resultCode == RESULT_OK) {
            if (getActivity() == null) return;

            Injector.get().userManager().clear();

            ((MainActivity) getActivity()).getViewModel().clear();
            ((BaseActivity) getActivity()).gotoActivity(ShowsActivity.class, true);
        }
    }

}
