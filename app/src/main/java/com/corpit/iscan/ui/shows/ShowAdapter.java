package com.corpit.iscan.ui.shows;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.corpit.iscan.databinding.RowShowBinding;
import com.corpit.iscan.model.Show;

import java.util.List;

public class ShowAdapter extends RecyclerView.Adapter<ShowAdapter.ShowHolder> {

    private List<Show> mData;
    private ClickCallback<Show> mListener;

    ShowAdapter(List<Show> data, ClickCallback<Show> listener) {
        this.mData = data;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ShowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RowShowBinding binding = RowShowBinding.inflate(LayoutInflater.from(parent.getContext()));
        return new ShowHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowHolder holder, int position) {
        final Show show = mData.get(position);
        holder.binding.setShow(show);
        holder.binding.setListener(mListener);

        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ShowHolder extends RecyclerView.ViewHolder {

        RowShowBinding binding;

        ShowHolder(RowShowBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
