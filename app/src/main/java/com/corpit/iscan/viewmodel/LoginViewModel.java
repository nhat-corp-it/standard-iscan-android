package com.corpit.iscan.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.data.remote.IScanClient;
import com.corpit.iscan.data.remote.IScanFieldMaps;
import com.corpit.iscan.data.remote.response.IScanResponse;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.global.AppExecutors;
import com.corpit.iscan.global.IScanApp;
import com.corpit.iscan.model.Action;
import com.corpit.iscan.model.ExhibitorProduct;
import com.corpit.iscan.model.Guide;
import com.corpit.iscan.model.Product;
import com.corpit.iscan.model.Show;
import com.corpit.iscan.model.User;
import com.corpit.iscan.model.User.UserType;
import com.corpit.iscan.ui.login.LoginNavigator;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends AndroidViewModel {

    private final MediatorLiveData<Boolean> loading;

    private final AppDatabase db;

    private final AppExecutors appExecutors;

    private WeakReference<LoginNavigator> mNavigator;

    public LoginViewModel(@NonNull Application application) {
        super(application);

        appExecutors = ((IScanApp) application).getAppExecutors();

        db = AppDatabase.getInstance(application);

        loading = new MediatorLiveData<>();
        loading.setValue(false);

    }

    public void setNavigator(LoginNavigator navigator) {
        mNavigator = new WeakReference<>(navigator);
    }

    public MediatorLiveData<Boolean> getLoading() {
        return loading;
    }

    public void login(final User user, final Show show) {
        loading.setValue(true);
        Call<IScanResponse<Object>> call = IScanClient.getApiService()
                .login(IScanFieldMaps.login(user.getType(), user.getQrCode()));

        call.enqueue(new Callback<IScanResponse<Object>>() {
            @Override
            public void onResponse(@NonNull Call<IScanResponse<Object>> call,
                                   @NonNull Response<IScanResponse<Object>> response) {
                loading.setValue(false);
                if (response.code() == 200 && response.body() != null) {
                    if (response.body().getStatus().equals("200")) {
                        Injector.get().userManager().login(user, show);
                        download(user);
                    } else {
                        if (mNavigator != null && mNavigator.get() != null) {
                            mNavigator.get().showLoginFailedDialog(response.body().getMessage());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<IScanResponse<Object>> call, @NonNull Throwable t) {
                loading.setValue(false);
                if (mNavigator != null && mNavigator.get() != null) {
                    mNavigator.get().showLoginFailedDialog(t.getMessage());
                }
            }
        });
    }

    public void download(final User user) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    loading.postValue(true);
                    final Response<IScanResponse<Action>> actionResponse = IScanClient.getApiService()
                            .getActions(IScanFieldMaps.actions()).execute();

                    appExecutors.diskIO().execute(new Runnable() {
                        @Override
                        public void run() {
                            List<Action> actionList = actionResponse.body().getData();
                            Action[] actions = actionList.toArray(new Action[actionList.size()]);
                            db.actionDao().insert(actions);
                        }
                    });

                    if (user.getType().equals(UserType.EXHIBITOR)) {
                        final Response<IScanResponse<ExhibitorProduct>> response = IScanClient.getApiService()
                                .getExhibitorProducts(IScanFieldMaps.exhibitorProducts(user.getCompanyId())).execute();

                        appExecutors.diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                List<ExhibitorProduct> exhibitorProductList = response.body().getData();

                                for (ExhibitorProduct product : exhibitorProductList) {
                                    product.setCompanyId(user.getCompanyId());
                                }

                                ExhibitorProduct[] exhibitorProducts = exhibitorProductList
                                        .toArray(new ExhibitorProduct[exhibitorProductList.size()]);
                                db.exhibitorProductDao().insert(exhibitorProducts);
                            }
                        });
                    }

                    if (user.getType().equals(UserType.VISITOR)) {
                        final Response<IScanResponse<Product>> productResponse = IScanClient.getApiService()
                                .getProducts(IScanFieldMaps.products()).execute();

                        appExecutors.diskIO().execute(new Runnable() {
                            @Override
                            public void run() {
                                List<Product> productList = productResponse.body().getData();
                                Product[] products = productList.toArray(new Product[productList.size()]);
                                db.productDao().insert(products);
                            }
                        });
                    }

                    Response<IScanResponse<Guide>> guideResponse = IScanClient.getApiService()
                            .getGuide(IScanFieldMaps.guide(user.getType())).execute();
                    List<Guide> guides = guideResponse.body().getData();
                    if (guides.size() > 0) {
                        Guide guide = guides.get(0);
                        db.saveGuide(guide);
                    }
                    appExecutors.mainThread().execute(new Runnable() {
                        @Override
                        public void run() {
                            if (mNavigator != null && mNavigator.get() != null) {
                                mNavigator.get().gotoMainActivity();
                            }
                        }
                    });
                } catch (final IOException e) {
                    appExecutors.mainThread().execute(new Runnable() {
                        @Override
                        public void run() {
                            if (mNavigator != null && mNavigator.get() != null) {
                                mNavigator.get().showDownloadFailedSnackBar(e.getMessage());
                            }
                        }
                    });
                } finally {
                    loading.postValue(false);
                }
            }
        };
        appExecutors.networkIO().execute(runnable);
    }
}
