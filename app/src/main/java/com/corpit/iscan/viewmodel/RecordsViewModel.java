package com.corpit.iscan.viewmodel;

import android.annotation.SuppressLint;
import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.ViewModelProvider;
import android.content.Context;
import android.support.annotation.NonNull;

import com.corpit.iscan.R;
import com.corpit.iscan.data.RecordRepository;
import com.corpit.iscan.data.RecordsDataSource;
import com.corpit.iscan.data.RecordsDataSource.Status;
import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.data.db.dao.BoothDao;
import com.corpit.iscan.data.db.dao.VisitorDao;
import com.corpit.iscan.global.AppExecutors;
import com.corpit.iscan.global.IScanApp;
import com.corpit.iscan.model.Booth;
import com.corpit.iscan.model.Visitor;
import com.corpit.iscan.sync.SyncWorker;
import com.corpit.iscan.ui.base.IRecord;
import com.corpit.iscan.ui.history.BoothActivity;
import com.corpit.iscan.ui.history.VisitorActivity;
import com.corpit.iscan.ui.main.MainActivityNavigator;
import com.corpit.iscan.widget.SingleLiveEvent;

import java.lang.ref.WeakReference;
import java.util.List;

import static com.corpit.iscan.ui.history.BoothActivity.EDIT_BOOTH_RESULT_OK;
import static com.corpit.iscan.ui.history.BoothActivity.NEW_BOOTH_RESULT_OK;
import static com.corpit.iscan.ui.history.VisitorActivity.EDIT_VISITOR_RESULT_OK;
import static com.corpit.iscan.ui.history.VisitorActivity.NEW_VISITOR_RESULT_OK;

public class RecordsViewModel<T extends IRecord> extends AndroidViewModel
        implements RecordsDataSource.CheckRecordCallback, RecordsDataSource.SyncCallback,
        RecordsDataSource.DownloadCallback {

    private static final int PAGE_HISTORY = 1;

    private RecordRepository<T> repository;

    private final SingleLiveEvent<String> snackbarMessage = new SingleLiveEvent<>();

    private final MediatorLiveData<Boolean> loading = new MediatorLiveData<>();

    private WeakReference<MainActivityNavigator> mMainNavigator;

    @SuppressLint("StaticFieldLeak")
    // Application context doesn't leak
    private final Context mContext;

    private Class<T> cls;

    public RecordsViewModel(@NonNull Application application, @NonNull Class<T> cls) {
        super(application);

        this.cls = cls;

        mContext = application.getApplicationContext();

        repository = getRepository(application);

        loading.setValue(false);
    }

    private RecordRepository<T> getRepository(Application application) {
        AppExecutors appExecutors = ((IScanApp) application).getAppExecutors();

        if (cls.equals(Visitor.class)) {
            VisitorDao visitorDao = AppDatabase.getInstance(mContext).visitorDao();
            return RecordRepository.getInstance(appExecutors, visitorDao);
        }

        if (cls.equals(Booth.class)) {
            BoothDao boothDao = AppDatabase.getInstance(mContext).boothDao();
            return RecordRepository.getInstance(appExecutors, boothDao);
        }

        return null;
    }

    public SingleLiveEvent<String> getSnackbarMessage() {
        return snackbarMessage;
    }

    public MediatorLiveData<Boolean> getLoading() {
        return loading;
    }

    public void setMainNavigator(MainActivityNavigator navigator) {
        mMainNavigator = new WeakReference<>(navigator);
    }

    public void handleOnActivityResult(int requestCode, int resultCode) {
        if (requestCode == VisitorActivity.REQUEST_CODE || requestCode == BoothActivity.REQUEST_CODE) {
            switch (resultCode) {
                case NEW_VISITOR_RESULT_OK:
                case NEW_BOOTH_RESULT_OK:
                case EDIT_VISITOR_RESULT_OK:
                case EDIT_BOOTH_RESULT_OK:
                    showMessage(resultCode);
                    if (mMainNavigator != null && mMainNavigator.get() != null) {
                        mMainNavigator.get().showPage(PAGE_HISTORY);
                    }
                    SyncWorker.attemptToSync(mContext);
                    break;
            }
        }
    }

    private void showMessage(int resultCode) {
        String message;
        switch (resultCode) {
            case NEW_VISITOR_RESULT_OK:
                message = mContext.getResources().getString(R.string.msg_new_visitor);
                break;
            case NEW_BOOTH_RESULT_OK:
                message = mContext.getString(R.string.msg_new_booth);
                break;
            case EDIT_VISITOR_RESULT_OK:
                message = mContext.getString(R.string.msg_edit_visitor);
                break;
            case EDIT_BOOTH_RESULT_OK:
                message = mContext.getString(R.string.msg_edit_booth);
                break;
            default:
                message = "";
                break;
        }

        snackbarMessage.setValue(message);
    }

    public LiveData<List<T>> getData() {
        return repository.getRecords();
    }

    public void checkRecord(T record) {
        repository.checkRecord(record, this);
    }

    public LiveData<Integer> getTotal() {
        return repository.getTotal();
    }

    public LiveData<Integer> getSent() {
        return repository.getSent();
    }

    public void onSyncButtonClick() {
        loading.setValue(true);
        repository.sync(this, cls);
    }

    public void onRefreshButtonClick() {
        loading.setValue(true);
        repository.download(AppDatabase.getInstance(mContext), this);
    }

    public void delete(long id) {
        repository.delete(id);
    }

    public void insert(T record) {
        repository.insert(record);
    }

    public void onSaveButtonClick(T record) {
        repository.update(record);
    }

    @Override
    public void onRecordExisted(IRecord record) {
        if (mMainNavigator != null && mMainNavigator.get() != null) {
            mMainNavigator.get().showUpdateDialog(record, cls);
        }
    }

    @Override
    public void onNewRecord(IRecord record) {
        if (mMainNavigator != null && mMainNavigator.get() != null) {
            mMainNavigator.get().gotoEditActivity(record, cls);
        }
    }

    public void onUpdateButtonClick(IRecord record) {
        if (mMainNavigator != null && mMainNavigator.get() != null) {
            mMainNavigator.get().gotoEditActivity(record, cls);
        }
    }

    @Override
    public void onSyncFinished(@Status int status) {
        loading.setValue(false);
        switch (status) {
            case Status.SUCCESS:
                snackbarMessage.setValue(mContext.getString(R.string.msg_synced));
                break;
            case Status.DISCONNECTED:
                snackbarMessage.setValue(mContext.getString(R.string.error_unknown_host));
                break;
            case Status.NO_NETWORK:
                snackbarMessage.setValue(mContext.getString(R.string.error_no_network));
                break;
            case Status.BAD_URL:
                snackbarMessage.setValue(mContext.getString(R.string.error_wrong_arg));
                break;
            case Status.NOT_A_RECORD:
                snackbarMessage.setValue(mContext.getString(R.string.error_wrong_type));
                break;
            case Status.UNKNOWN:
                snackbarMessage.setValue(mContext.getString(R.string.error_unknown));
                break;
        }
    }

    @Override
    public void onDownloadComplete(@Status int status) {
        loading.setValue(false);
        switch (status) {
            case Status.SUCCESS:
                snackbarMessage.postValue(mContext.getString(R.string.msg_refresh));
                break;
            case Status.DISCONNECTED:
                snackbarMessage.setValue(mContext.getString(R.string.error_unknown_host));
                break;
            case Status.NO_NETWORK:
                snackbarMessage.setValue(mContext.getString(R.string.error_no_network));
                break;
            case Status.BAD_URL:
                snackbarMessage.setValue(mContext.getString(R.string.error_wrong_arg));
                break;
            case Status.NOT_A_RECORD:
                snackbarMessage.setValue(mContext.getString(R.string.error_wrong_type));
                break;
            case Status.UNKNOWN:
                snackbarMessage.setValue(mContext.getString(R.string.error_unknown));
                break;
        }
    }

    public static class Factory implements ViewModelProvider.Factory {
        private final Application application;
        private final Class cls;

        public Factory(Application application, Class cls) {
            this.application = application;
            this.cls = cls;
        }

        @NonNull
        @SuppressWarnings("unchecked")
        @Override
        public RecordsViewModel create(@NonNull Class modelClass) {
            return new RecordsViewModel(application, cls);
        }
    }

    public void clear() {
        repository.reset();
    }
}
