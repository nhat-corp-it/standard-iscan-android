package com.corpit.iscan.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;

import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.di.Injector;
import com.corpit.iscan.global.IScanApp;

import java.util.List;

public class FollowUpViewModel extends AndroidViewModel {

    private final MediatorLiveData<List<String>> products;

    private final MediatorLiveData<List<String>> actions;

    public FollowUpViewModel(@NonNull Application application) {
        super(application);

        products = new MediatorLiveData<>();
        products.setValue(null);

        actions = new MediatorLiveData<>();
        actions.setValue(null);

        final AppDatabase db = ((IScanApp) application).getDatabase();

        ((IScanApp) application).getAppExecutors().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                String companyId = Injector.get().userManager().getUser().getCompanyId();
                List<String> productList = db.exhibitorProductDao().loadAll(companyId);
                products.postValue(productList);

                List<String> actionList = db.actionDao().loadAll();
                actions.postValue(actionList);

            }
        });
    }

    public MediatorLiveData<List<String>> getProducts() {
        return products;
    }

    public MediatorLiveData<List<String>> getActions() {
        return actions;
    }
}
