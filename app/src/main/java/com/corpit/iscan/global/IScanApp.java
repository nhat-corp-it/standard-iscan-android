package com.corpit.iscan.global;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.corpit.iscan.data.db.AppDatabase;
import com.corpit.iscan.di.AppComponent;
import com.corpit.iscan.di.DaggerAppComponent;
import com.corpit.iscan.di.module.AppModule;
import com.corpit.iscan.di.module.DatabaseModule;
import com.corpit.iscan.di.module.ServiceModule;
import com.corpit.iscan.di.module.UserModule;

public class IScanApp extends Application {

    private AppExecutors appExecutors;

    private AppComponent component;

    private static IScanApp INSTANCE;

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        appExecutors = new AppExecutors();

        INSTANCE = this;
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .databaseModule(new DatabaseModule())
                .serviceModule(new ServiceModule())
                .userModule(new UserModule())
                .build();
    }

    public AppComponent getComponent() {
        return component;
    }

    public static IScanApp get() {
        return INSTANCE;
    }

    public AppDatabase getDatabase() {
        return AppDatabase.getInstance(this);
    }

    public AppExecutors getAppExecutors() {
        return appExecutors;
    }
}
