package com.corpit.iscan.global;

import android.content.SharedPreferences;
import android.text.TextUtils;

import com.corpit.iscan.model.Show;
import com.corpit.iscan.model.User;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;

public class UserManager {

    private static final String KEY_IS_LOGIN = "isLogin";
    private static final String KEY_ROLE = "role";
    private static final String KEY_INFO = "info";
    private static final String KEY_SHOW_ID = "showId";
    private static final String KEY_SHOW_ENC_KEY = "encKey";
    private static final String KEY_SHOW_BANNER = "showBanner";

    @User.UserType
    private String mType;

    private User mUser;

    private String showId;
    private String showEncKey;
    private String showBanner;

    private boolean isLogin = false;
    private SharedPreferences sharedPref;

    public UserManager(SharedPreferences sharedPreferences) {
        sharedPref = sharedPreferences;
        loadUser();
    }

    public String getType() {
        return mType;
    }

    public boolean isLogin() {
        return isLogin;
    }

    private void cacheUser() {
        sharedPref.edit().putBoolean(KEY_IS_LOGIN, isLogin).apply();

        sharedPref.edit().putString(KEY_ROLE, mType).apply();

        sharedPref.edit().putString(KEY_SHOW_ID, showId).apply();
        sharedPref.edit().putString(KEY_SHOW_ENC_KEY, showEncKey).apply();
        sharedPref.edit().putString(KEY_SHOW_BANNER, showBanner).apply();

        JsonAdapter<User> jsonAdapter = new Moshi.Builder().build().adapter(User.class);
        String info = jsonAdapter.toJson(mUser);
        sharedPref.edit().putString(KEY_INFO, info).apply();
    }

    private void loadUser() {
        isLogin = sharedPref.getBoolean(KEY_IS_LOGIN, false);
        if (!isLogin) return;

        mType = sharedPref.getString(KEY_ROLE, null);
        if (TextUtils.isEmpty(mType)) return;

        showId = sharedPref.getString(KEY_SHOW_ID, null);
        showEncKey = sharedPref.getString(KEY_SHOW_ENC_KEY, null);
        showBanner = sharedPref.getString(KEY_SHOW_BANNER, null);

        JsonAdapter<User> jsonAdapter = new Moshi.Builder().build().adapter(User.class);
        String info = sharedPref.getString(KEY_INFO, "");
        try {
            mUser = jsonAdapter.fromJson(info);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clear() {
        sharedPref.edit().clear().apply();
        mType = null;
        mUser = null;
        showId = null;
        showEncKey = null;
        showBanner = null;
        isLogin = false;
    }

    public void login(User user) {
        mUser = user;
        mType = user.getType();
        isLogin = true;
        cacheUser();
    }

    public void login(User user, Show show) {
        mUser = user;
        mType = user.getType();
        showId = show.getShowID();
        showEncKey = show.getShowEncryptedKey();
        showBanner = show.getShowLogo();
        isLogin = true;
        cacheUser();
    }

    public User getUser() {
        return mUser;
    }

    public String getShowId() {
        return showId;
    }

    public String getShowEncKey() {
        return showEncKey;
    }

    public String getShowBanner() {
        return showBanner;
    }

    public void setShowId(String showId) {
        this.showId = showId;
    }

    public void setShowEncKey(String showEncKey) {
        this.showEncKey = showEncKey;
    }
}
